const fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
const ID = "AKIAZT3OYKDLPC3VECJN";
const SECRET = "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";
const BUCKET_NAME = "cypress-video-automation";
const axios = require("axios");
const locations = [];
let results;
let url
const MongoClient = require("mongodb").MongoClient;
let storeName;
let errorDisplay;
let logError;

function uploadToS3(file, name, type, format) {

  const s3bucket = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET,
    Bucket: BUCKET_NAME,
  });

  const params = {
    Bucket: BUCKET_NAME,
    Key: name,
    Body: file,
    ACL: "public-read",
    ContentType: `${type}/${format}`,
  };

  s3bucket.upload(params, (err, data) => {
    if (err) throw err;
    locations.push(data["Location"]);
    requestDataS3(locations);
  });
}

function send(store, urlCypress) {
  storeName = store;
  url = urlCypress
  uploadScreenshots();
  uploadVideos();
}

function requestDataS3() {

  if (locations.length === 2) {
    MongoClient.connect(
      "mongodb+srv://admin:admin@cluster0.qmjwd.mongodb.net/cypress-automation",
      (err, client) => {
        if (err) {
          console.log(`MONGO CONNECTION ERROR: ${err}`);
          throw err;
        } else {
          const db = client.db("cypress_automation");
          
          db.collection("record")
            .find({ store: storeName })
            .sort({ date: -1 })
            .toArray(function (error, docs) {
              results = docs[0];

              if (error) throw error;
              console.log("DOCS");

              let video = locations.filter((s) => s.includes(".mp4"))[0];
              let image = locations.filter((s) => s.includes(".png"))[0];

              console.log('storeName',storeName)
              
              let size = Object.keys(results.tests).length;

              let datesNow = new Date();
              (dia = datesNow.getDate().toString().padStart(2, "0")),
                (mes = (datesNow.getMonth() + 1).toString().padStart(2, "0")),
                (ano = datesNow.getFullYear());
              let date = dia + "/" + mes + "/" + ano;

              console.log(date);
              // let hoursNow = new Date();
              let hoursNow = new Date();

              let otherHourDocker = new Date(hoursNow);

              otherHourDocker.setHours(otherHourDocker.getHours() + 3); // Adiciona 2 horas

              var data = new Date();
              data.getHours(); // 9
              data.setHours(data.getHours() - 3);
              data.getHours(); // 12
              let time = data.toLocaleTimeString();

              let axiosArray = [];
              let postData = {};
              let cont = 0;

              console.log("DADOS3");

              while (cont < size) {
                console.log("ENTREI");

                if (results.tests[cont].state === "passed") {
                  console.log("IF");
                  cont++;
                } else {
                  errorDisplay = results.tests[cont].displayError;

                  if (
                    errorDisplay.includes(
                      "Não foi possível encontrar CEP disponível"
                    )
                  ) {
                    console.log("IF 1");

                    logError =
                      results.tests[cont].displayError.slice(7, 48) + "!";

                    postData["errorMessage"] = logError;
                  } else if (
                    errorDisplay.includes(
                      "Não foi possível encontrar nenhum produto"
                    )
                  ) {
                    console.log("IF 2");

                    logError =
                      results.tests[cont].displayError.slice(7, 48) + "!";

                    postData["errorMessage"] = logError;

                    console.log(logError, postData["errorMessage"]);
                  } else {
                    console.log("IF 3");

                    postData["errorMessage"] =
                      results.tests[cont].displayError.slice(0, 100) + "...";
                  }

                  postData["store"] = `${storeName}`;
                  postData["date"] = date;
                  postData["hour"] = time;
                  postData["video"] = video;
                  postData["image"] = image;
                  postData["url"] = url
                  let newPromise = axios({
                    method: "post",
                    url:
                      "https://3i8ivbxg77.execute-api.us-east-1.amazonaws.com/api/messaging",
                    data: postData,
                  });

                  cont++;
                  axiosArray.push(newPromise);
                }
                console.log("post",postData);
              }
              axios
                .all(axiosArray)
                .then(
                  axios.spread((...responses) => {
                    responses.forEach((res) => console.log("Success"));
                    process
                      .exit(0)
                      .then((data) => console.log("data", data))
                      .catch((error) => {
                        if (error) throw ("ERROU", error);
                      });
                  })
                )
                .catch((error) => {
                  if (error) throw ("ERROU", error);
                });
            });
        }
      }
    );
  }
}

function getFiles(dir, fileList = []) {
  const files = fs.readdirSync(dir);
  files.forEach((file) => {
    const filePath = `${dir}/${file}`;
    if ([".gitkeep", ".Trash-0", "assets"].indexOf(file) === -1) {
      if (fs.statSync(filePath).isDirectory()) {
        getFiles(filePath, fileList);
      } else {
        const obj = {
          path: filePath,
          name: file,
          format: "image",
          type: file.split(".")[1],
        };
        fileList.push(obj);
      }
    }
  });
  return fileList;
}

function uploadVideos() {
  const videosDir = path.join(__dirname, "..", "cypress", "videos");
  const videos = getFiles(videosDir, []);
  
  console.log('VIDEOS', videos[0].path)

  fs.readFile(videos[0].path, (err, data) => {
    if (err) throw err;
    if (err) throw err;
    let dates = new Date()
     uploadToS3(data, `${dates.toString()} ${videos[0].name}`, "video", videos[0].type);
  });
}

function uploadScreenshots() {
  const screenshotsDir = path.join(__dirname, "..", "cypress", "screenshots");

  const screenshots = getFiles(screenshotsDir, []);
  fs.readFile(screenshots[0].path, (err, data) => {
    if (err) throw err;
    let dates = new Date()

    uploadToS3(data, `${screenshots[0].name} ${dates.toString()}`, "image", screenshots[0].type);
  });
}

function uploadMochaAwesome() {
  const reportsDir = path.join(__dirname, "..", "mochareports");
  const report = getFiles(reportsDir, [])[0];
  fs.readFile(report.path, (err, data) => {
    if (err) throw err;
    uploadToS3(data, report.name, report.type);
  });
}

module.exports = {
  uploadVideos,
  uploadScreenshots,
  uploadMochaAwesome,
  requestDataS3,
  send,
};
