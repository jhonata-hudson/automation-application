const path = require("path");
const cypress = require("cypress");
const uuidv1 = require("uuid/v1");
const upload = require("../upload.js");
const combine = require("../combine.js");

const local = process.argv[2];

cypress
  .run({
    spec: ["cypress/integration/tests/ambev/ambevRegression.spec.ts"],
    browser: "firefox",
    config: {
      video: true,
      videoCompression: 32,
      chromeWebSecurity: true,
      defaultCommandTimeout: 120000,
      pageLoadTimeout: 1200000,
    },
    record: true,
    key: "a943e621-6152-4793-b8c2-13b10a6d694d",
    tag: "Ambev",
  })
  .then((res) => {
    
    let cypressUrl = res.runUrl
        
   if (res.totalFailed >= 1) {
      
     upload.send("AMBEV",cypressUrl);
 
   } else {
     console.log("\nLocal run - skipping Sp3 upload");
   }
  })
  .catch((err) => {
    console.error(err);
  });
