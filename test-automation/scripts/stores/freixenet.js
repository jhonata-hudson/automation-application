const path = require("path");
const cypress = require("cypress");
const uuidv1 = require("uuid/v1");
const upload = require("../upload.js");
const combine = require("../combine.js");

const local = process.argv[2];

cypress
  .run({
    spec: ["cypress/integration/tests/freixenet/freixenetRegression.spec.ts"],
    browser: "firefox",
    config: {
      video: true,
      videoCompression: 32,
      defaultCommandTimeout: 120000,
      chromeWebSecurity: true
    },
     record: true,
     key: "a943e621-6152-4793-b8c2-13b10a6d694d",
     tag: "Freixenet",
  })
  .then((res) => {
    let cypressUrl = res.runUrl

    // rimraf(path.join(__dirname, "..", "reports"), () => {});
   if(res.totalFailed >= 1) {
      upload.send('FREIXENET',cypressUrl);
    } else {
      console.log("\nLocal run - skipping Sp3 upload");
    }
  })
  .catch((err) => {
    console.error(err);
  });
