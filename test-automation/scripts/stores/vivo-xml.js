const path = require("path");
const cypress = require("cypress");
const uuidv1 = require("uuid/v1");
const upload = require("../upload.js");
const combine = require("../combine.js");

const local = process.argv[2];

cypress
  .run({
    spec: ["cypress/integration/tests/vivoXml/vivo-Xml.spec.ts"],
    browser: "firefox",
    config: {
      video: true,
      videoCompression: 32,
      chromeWebSecurity: false
    },
    record: true,
    key: "a943e621-6152-4793-b8c2-13b10a6d694d",
    tag: "Vivo-XML",
  })
  .then((res) => {
    let cypressUrl = res.runUrl

    // rimraf(path.join(__dirname, "..", "reports"), () => {});
   if(res.totalFailed >= 1) {
   
      upload.send('VIVOXML',cypressUrl);
   
    } else {
      console.log("\nLocal run - skipping Sp3 upload");
    }
  })
  .catch((err) => {
    console.error(err);
  });
