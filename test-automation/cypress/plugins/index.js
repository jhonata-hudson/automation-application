var fs = require("fs");
const MongoClient = require("mongodb").MongoClient;

module.exports = (on, config) => {
  on("after:spec", (spec, results) => {

    let stores = results.spec.name.split("/");
    let store = results.spec.name.split("/");
    
    let storeName = stores[1].toUpperCase();
  
    console.log(storeName);
  
    return new Promise((resolve) => {
      MongoClient.connect(
        "mongodb+srv://admin:admin@cluster0.qmjwd.mongodb.net/cypress-automation",

        (err, client) => {
          console.log(`TESTE: ${client}`);

          if (err) {
            console.log(`MONGO CONNECTION ERROR: ${err}`);
            throw err;
          } else {
            const db = client.db("cypress_automation");

            console.log("DB");

            db.collection("record")
              .insertOne({
                ...{ tests: results.tests },
                ...{ store: storeName, date: new Date() },
              })
              .then((data) => {
                console.log("SUCESSO");
              });
          }
          resolve(true);
        }
      );
    });
  }),
    on("task", {
      insertCollectionData(data) {
        return new Promise((resolve) => {
          MongoClient.connect(
            "mongodb+srv://admin:admin@cluster0.qmjwd.mongodb.net/cypress-automation",
            (err, client) => {
              if (err) {
                console.log(`MONGO CONNECTION ERROR: ${err}`);
                throw err;
              } else {
                const db = client.db("cypress_automation");
                db.collection("xml").insertOne({
                  client: data.store,
                  xml: data.xml,
                  url: data.url,
                  indexedProducts: data.indexedProducts,
                  createDate: new Date(),
                });
              }
              resolve(true);
            }
          );
        }); // end of return Promise
      },
      findCollectionData(data) {
        return new Promise((resolve) => {
          MongoClient.connect(
            "mongodb+srv://admin:admin@cluster0.qmjwd.mongodb.net/cypress-automation",
            (err, client) => {
              if (err) {
                console.log(`MONGO CONNECTION ERROR: ${err}`);
                throw err;
              } else {
                const db = client.db("cypress_automation");
                db.collection("xml")
                  .find({ client: data.store, xml: data.xml })
                  .sort({ createDate: -1 })
                  .toArray(function (error, docs) {
                    resolve(docs);
                  });
              }
            }
          );
        }); // end of return Promise
      },
      log(message) {
        console.log(message);
        return null
      }
    });
  return config;
};
