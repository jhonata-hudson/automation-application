declare namespace Cypress {
  interface Chainable<Subject> {
    createInbox(): Cypress.Chainable;
    waitForLatestEmail(value: String): Cypress.Chainable;
    getInbox(value: String): Cypress.Chainable;
    before(value: String) : Cypress.Chainable;
  }
}
