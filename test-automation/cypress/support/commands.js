const { MailSlurp } = require("mailslurp-client");

function unquote(str) {
  return str.replace(/(^")|("$)/g, "");
}

Cypress.Commands.add("before", { prevSubject: "element" }, (el, property) => {
  const win = el[0].ownerDocument.defaultView;
  const before = win.getComputedStyle(el[0], "before");
  return unquote(before.getPropertyValue(property));
});

const apiKey =
  "5b4f385452287a694109a8635e9de8b97234f9f4b6f683eae8c7b623b27938f4";

const mailslurp = new MailSlurp({ apiKey });


Cypress.Commands.overwrite('log', (subject, message) => cy.task('log', message));


Cypress.Commands.add("waitForLatestEmail", (inboxId) => {
  return mailslurp.waitForLatestEmail(inboxId);
});


Cypress.Commands.add("waitForLatestEmail", (inboxId) => {
  return mailslurp.waitForLatestEmail(inboxId);
});

Cypress.Commands.add("getInbox", (emailId) => {
  return mailslurp.getInbox(emailId);
});

Cypress.Commands.add("createInbox", () => {
  // instantiate MailSlurp
  const mailslurp = new MailSlurp({ apiKey });
  // return { id, emailAddress } or a new randomly generated inbox
  return mailslurp.createInbox();
});
