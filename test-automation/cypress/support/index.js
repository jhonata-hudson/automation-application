// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:

//import ("./commands")
import addContext from 'mochawesome/addContext'


const titleToFileName = (title) => title.replace(/[:\/]/g, '');

Cypress.on('test:after:run', (test, runnable) => {
    if (test.state === 'failed') {
        const filename = `${titleToFileName(runnable.parent.title)} -- ${titleToFileName(test.title)} (failed).png`;
        addContext({ test }, `../screenshots/${Cypress.spec.name}/${filename}`);
        addContext({ test }, `../videos/${Cypress.spec.name}.mp4`);
    }
})



Cypress.on("window:before:load", function (win) {
  const customCommands = require("./commands"); 

  module.exports = {
    commands: customCommands,
  };

  const original = win.EventTarget.prototype.addEventListener;

  win.EventTarget.prototype.addEventListener = function () {
    if (arguments && arguments[0] === "beforeunload") {
      return;
    }
    return original.apply(this, arguments);
  };

  Object.defineProperty(win, "onbeforeunload", {
    get: function () {},
    set: function () {},
  });

  const docIframe = window.parent.document.getElementById("Your App: 'cypress'");

  const appWindow = docIframe.contentWindow;

  ['log', 'info', 'error', 'warn', 'debug'].forEach((consoleProperty) => {
    appWindow.console[consoleProperty] = function (...args) {
   
       logs += args.join(' ') + '\n';
     };
  });



});
// Alternatively you can use CommonJS syntax:
// require('./commands')
