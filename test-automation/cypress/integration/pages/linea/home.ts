export const elements: any = {
  logo: ".j-logo-small",
  btnMyAcount: ".j-main-header__right--box-links  > a",
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  filterCategories: ".search-single-navigator > ul > li",
  linkCategoriesExact: ".j-main-header__middle-menu.js--menu-content > ul > li",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  cookies: ".j-cookieAlert > button",
  fieldBlack: ".cl_email.email",
  acceptBlack: ".newsletter-button-ok",
  nomeModal: ".j-modal-newsLetter__register--form__name",
  emailModal: ".j-modal-newsLetter__register--form__email",
  modalForm: ".j-modal-newsLetter__register--form",
  acceptAftModal: ".swal2-confirm.swal2-styled",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class HomeLinea {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnMyAcount)
      .invoke("show")
      .contains("Minha Conta")
      .click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
          cy.get(elements.loginOk).should("have.text", "Olá");
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.btnMyAcount)
      .invoke("show")
      .contains("Minha Conta")
      .click({ force: true });
    cy.get(elements.btnloginEmailAndPassword).click({ force: true });
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });
    cy.wait(3000);
    cy.get("body").then((url) => {
      let ola = url.find(elements.alert).length;
      cy.log(String(ola));
      if (ola >= 1) {
        this.validNegative();
      } else if (ola === 0) {
        if (pass === "invalidPass" || email === "invalidEmail")
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!");
        cy.get(elements.loginOk).should("have.text", "Olá");
        cy.get(elements.logo).invoke("show").click({ force: true });
      }
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static modalHome() {
    cy.get(elements.modalForm).within((body) => {
      cy.get(elements.nomeModal).type("teste teste");
      cy.get(elements.emailModal).type("teste@teste.com.br");
      let radios = body.find("label").length;
      let idx = Math.floor(Math.random() * radios);
      cy.get("label").eq(idx).click();
      cy.get("button").contains("Cadastrar").click();
    });
    cy.get(elements.acceptAftModal).click();
  }

  static acceptCookie() {
    cy.get(elements.cookies).click({ timeout: 30000 });
  }

  static accessCategoryAndSubCategory(): void {
    cy.get(`${elements.linkCategoriesExact} > a`)
      .first()
      .click({ force: true });
  }
}
