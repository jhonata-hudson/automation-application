export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-img__img > img",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  productItem: ".j-shelf__item-content > .j-shelf__item-buy > .j-shelf__item-buy--cta",
  btnBuyProducts: ".popup-compra--info__btn > a",
};

export class Product {
  static selectProduct(): void {
  
    cy.get(elements.productItem)
      .its("length")
      .then((length) => {
        cy.get(elements.productItem)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
      });
  }

  static goCartOrKeepBuying(): void {
    cy.wait(2000);

    cy.get("body").then((el) => {

      let btnBuy = el.find(elements.btnBuyProducts).length;
      
      cy.log(String(btnBuy))

      if (btnBuy > 0) {
        cy.get(elements.btnBuyProducts)
          .contains("Ir para o carrinho")
          .invoke("removeAttr", "target")
          .click({ force: true });
      } else {
        cy.get(".buy-button.buy-button-ref")
          .invoke("removeAttr", "target")
          .click({ force: true });
      }
    });
  }
}
