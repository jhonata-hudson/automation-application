import { Commons } from './../commons';
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  calculeCep: ".srp-data > #shipping-calculate-link"

};

export class Checkout {
  static finish(logged: Boolean = true): void {
    Commons.inputCepAleatory(5,elements.inputCep,elements.calculeCep,true);  
    cy.get(elements.btnFinish, { timeout: 3000 }).click();
    if (logged) {
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      )
    } else {
      cy.get(elements.fieldEmail).type("teste@teste.com.br", { force: true });
      cy.get(elements.btnSendMail).click();
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      )
    }
  }

  static loggedCheck() {
    cy.wait(2000);
    cy.get(elements.inputFirstName, { timeout: 3000 }).type("Teste", {
      timeout: 3000,
    });
    cy.get(elements.inputLastName, { timeout: 3000 }).type("Teste", {
      timeout: 3000,
    });
    cy.get(elements.inputCPF).type("565.554.030-47");
    cy.get(elements.inputCellPhone).type("11999887766");
    cy.get(elements.btnShipping).click();
    cy.get(elements.inputNumberHouse).type(
      `${Math.floor(Math.random() * 1000)}`
    );
  }
}
