import { Home } from "../avenida/home";

export const elements: any = {
  productItem: ".j-shelf__item-img > a",
  btnWishList: ".btn.btn-block.btn-outline-secondary.y-wishBtn.btn-wishlist",
  selectModel: ".group_0",
  selectModelLabel: ".sku-selector-container",
  selectModelSpan: ".sku-selector-container > ul li span",
  btnBuyProduct: ".buy-button.buy-button-ref",
  divBuyInfo: ".popup-compra--info__btn",
  search: 'input[class="__rh-sb-input__ rh-sb-input"]',
  unavailable: 'p[class="unavailable-button"][style="display:block"]',
  shadow_root: 'main[class="main"] > impulse-search',
  productRH: ".impulse-card.impulse-product-card.product.grid",
};
export class Product {
  static selectProduct(retries?: any) {
    cy.log("ENTREI PARA SELECIONAR UM PRODUTO ----------");

    cy.url().then((url) => {
      console.log("URL----------");

      cy.get("body").then((body) => {
        let products = body.find(elements.productItem).length;

        cy.log(`QUANTIDADE DE PROTUDOS>> ${products}`);

        if (products === 0 && url.includes("?q") === false) {
          if (retries === 0) {
            throw "Não foi possível encontrar nenhum produto";
          }
          cy.go("back");
          Home.accessCategoryAndSubCategory();
          let products2 = products;
          if (products2 === 0) this.selectProduct(retries - 1);
        } else if (products === 0 && url.includes("?q")) {
          if (retries === 0) {
            throw "Não foi possível encontrar nenhum produto";
          }
          cy.get(elements.shadow_root)
            .shadow()
            .within((shadow) => {
              cy.log("SHADOW ENTREI +++++++++++++");

              cy.get('section[id="content"]', { timeout: 3000 })
                .should("exist")
                .within((els) => {
                  const sh = els.find(`${elements.productRH}`).length;
                  if (sh > 0) {
                    cy.get(elements.productRH)
                      .within((rh) => {
                        cy.get(`div[class="impulse-image"] > a`).as("teste");
                      })
                      .as("amount");
                  }
                })
                .as("amount");
            })
            .as("elements");
          cy.get("@elements").then((els) => {
            const len = els.find(elements.productRH).length;

            cy.log("OI +++++++++++++");

            if (len === 0) {
              cy.go("back");
              Home.accessCategoryAndSubCategory();
              return this.selectProduct(retries - 1);
            } else {
              cy.get("@teste")
                .eq(Math.floor(Math.random() * len))
                .click({ force: true });
              return false;
            }
          });
        } else {
          cy.get(elements.productItem)
            .its("length")
            .then((length) => {
              cy.get(elements.productItem)
                .eq(Math.floor(Math.random() * length))
                .click({ force: true });
            });
        }
      });
    });
    cy.log("SAI DO METODO DE SELEÇÃO DE PRODUTO +++++++++++++");
  }

  static wishList(): void {
    cy.get(elements.btnWishList).click();
  }

  static selectModelAndBuy(): void {
    cy.wait(3000);
    cy.get("body").then((body) => {
      if (body.find(elements.unavailable).length > 0) {
        cy.go("back");
        Home.accessCategoryAndSubCategory();
        this.selectProduct();
      }
      if (body.find(elements.selectModelLabel).length > 0) {
        let models = body.find(elements.selectModelLabel + `> ul li span label`)
          .length;
        if (models >= 1) {
          cy.get(elements.selectModelLabel)
            .its("length")
            .then((totalSizes) => {
              cy.get(elements.selectModelSpan).then((idx) => {
                if (idx.find('label[class$="disabled"]').length > 0) {
                  let allSizesUnavaible = idx.find('label[class$="disabled"]')
                    .length;
                  let sizesUnavaible = [];
                  let sizesAvaible = [];
                  for (let index = 0; index < allSizesUnavaible; index++) {
                    sizesUnavaible[index] = idx
                      .find('label[class$="disabled"]')
                      .eq(index)
                      .text();
                  }
                  for (let i = 0; i < totalSizes; i++) {
                    sizesAvaible[i] = idx.find("label").eq(i).text();
                  }
                  let toRemove = new Set(sizesUnavaible);

                  let sizesAvaibles = sizesAvaible.filter(
                    (x) => !toRemove.has(x)
                  );
                  sizesAvaibles.length > 1
                    ? cy
                        .get("label")
                        .contains(
                          sizesAvaibles[
                            Math.floor(Math.random() * sizesAvaibles.length)
                          ]
                        )
                        .click({ force: true })
                    : cy
                        .get("label")
                        .contains(`${sizesAvaibles[0]}`)
                        .click({ force: true });
                } else {
                  idx
                    .find("label")
                    .eq(Math.floor(Math.random() * length))
                    .click();
                }
              });
            });
        }
      }
    });
  }

  static goCartOrKeepBuying(cart: boolean = true): void {
    cy.log("VOU MANDAR PRO CARRINHO +++++++++++++");

    cy.get(elements.btnBuyProduct, { timeout: 10000 }).click({ force: true });

    cy.log("CLICKEI EM COMPRAR+++++++++++++");

    cy.wait(2000);

    cy.get("body").then((body) => {
      let modal = body.find(elements.divBuyInfo).length;

      if (modal >= 1) {
        cy.get(elements.divBuyInfo, { timeout: 10000 }).within(() => {
          cy.get("a").eq(1).click({ force: true });
        });

        cy.log(
          "CLICKEI EM MODAL DE CONFIRMAÇÃO DE ADICAO DE PRODUTO+++++++++++++"
        );
      }
    });
  }
}
