import { SendMail } from "./../commandsCustom";
import * as Mail from "../commandsCustom";
let profile: any;

export const elements: any = {
  spanWelcome: "#js--close",
  linkCategories: ".nav > ul > li > .mega-menu > .mega-menu__content",
  linkSubCategories: "a",
  btnLogin: '.j-main-header__right--box > [href="/account"]',
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnSingIn: "#classicLoginBtn",
  elCategoryUnavaible: "#busca-vazia-extra-middle",
  cookies: ".j-cookieAlert > button",
  loginOk: ".vtex-account__user-greeting > span",
  logo: ".j-main-header__items > .text-center > .logo > img",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
  calculeCep: "#shipping-calculate-link",
};
export class Home {
  static login(pass: string, email: string) {
    cy.log("ENTREI LOGIN ---------------")
    
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get("a").contains("Entre").click({ force: true });

    cy.log("CLICK LOGIN ---------------")

    cy.get(elements.btnloginEmailAndPassword).click();
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnSingIn).click({ force: true });
    cy.wait(5000);

    cy.log("LOGADO ---------------")

    cy.get("body").then((url) => {
      cy.url().then((urls) => {
        let profile = urls.includes("profile");
        if (profile) {
          if (pass === "invalidPass" || email === "invalidEmail")
            throw new Error(
              "FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!"
            );
          cy.get(elements.loginOk).should("have.text", "Olá");
            cy.log("LOGIN VALIDO---------------")
            
            cy.get(elements.logo).should("exist").click({ force: true });
          
        } else if (profile === false) {
        
          this.validNegative();
        }
      });
    });
  }
  static validCode(invalid?: string, email?: any, lastMail?: any) {
    let inboxId;
    cy.get(elements.btnLogin).click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      console.log("else1");

      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      console.log("else2");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      console.log("else3");

      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static closeSpan() {
    cy.get("body").then((res) => {
      if (res.find(elements.spanWelcome).length > 0)
        res.find(elements.spanWelcome).click();
    });

    cy.log("SPAN FECHADO ---------------")

  }
  static acceptCookie() {
    cy.get(elements.cookies).click({ timeout: 30000 });
    
    cy.log("COOKIE FECHADO ---------------")
  }

  static accessCategoryAndSubCategory(): void {
    
    cy.log("ENTREI PRIMEIRO METODO ---------------")
    
    cy.get(elements.linkCategories)
      .its("length")
      .then((length) => {

          cy.log("CONTEI AS CATEGORIAS")

        let idx = Math.floor(Math.random() * length);
        if (idx === 0) idx++ + 1;
        if (idx === 12) idx-- - 1;
        cy.get(elements.linkCategories)
          .eq(idx)
          .within(() => {
            cy.get(elements.linkSubCategories)
              .its("length")
              .then((length) => {
                cy.get(elements.linkSubCategories)
                  .eq(Math.floor(Math.random() * length))
                  .click({ force: true });

                  cy.log("SELECIONEI A CATEGORIA")

              });
          });
      });
  }
}
