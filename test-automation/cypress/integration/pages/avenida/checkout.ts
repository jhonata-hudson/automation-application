import { Commons } from "./../commons";

declare var require: any;
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  cep: "#ship-postalCode",
  inputNumberHouse: 'input[id="ship-number"]',
  btnFinishBuy: "#btn-go-to-payment",
  calculeCep: "#shipping-calculate-link",
  destiny: "#ship-receiverName",
};

export class Checkout {
  static finish(logged: string): void {
    
    Commons.inputCepAleatory(5, elements.cep, elements.calculeCep, true);


    cy.log('INICIANDO DADOS DE CHECKOUT ')
    
    cy.get(elements.btnFinish).should("exist").click({force:true});


    cy.log('CLIQUEI EM FINALIZAR COMPRAR NO CARRINHO -------------- ')

    if (logged === 'true') {

      cy.get(elements.inputClientEmail).should("exist").type(`teste@teste.com.br`);

      cy.log('PREENCHI O CAMPO EMAIL ---------------')
  
      cy.get(elements.btnClientEmail).should("exist").click({ force: true });
  
      cy.log('CLIQUEI EM CONFIRMAR EMAIL -------------- ')

      cy.wait(5000);
    
      cy.get(elements.btnContinueShopping, { timeout: 10000 }).should("exist").click({
        force: true,
      });
      cy.log('CLIQUEI EM CONTINUAR COMPRANDO -------------- ')

      cy.get(elements.inputNumberHouse).should("exist").type(
        `${Math.floor(Math.random() * 1)}`
      );
  
      cy.log('PREENCHI NUMERO DE CASA -------------- ')
  
      cy.get(elements.destiny, { timeout: 2000 }).should("exist").type("Teste", {
        force: true,
      });
      cy.get(elements.btnFinishBuy, { timeout: 2000 }).should("exist").click();
     
   
    } else {
     
      Commons.inputDataName([
        elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone,
      ]);
    }
  }

  static checkoutLogged() {
    cy.get(elements.inputFirstName, { timeout: 3000 }).should("exist").type("teste", {
      force: true,
    });
    cy.get(elements.inputLastName, { timeout: 3000 }).should("exist").type("teste", {
      force: true,
    });
    cy.get(elements.inputCPF, { timeout: 3000 }).should("exist").type("565.554.030-47", {
      force: true,
    });
    cy.get(elements.inputCellPhone, { timeout: 3000 }).should("exist").type("11999999999", {
      force: true,
    });
  }
}
