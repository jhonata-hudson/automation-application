import { Commons } from "./../commons";
import { Product } from "./../softEverest/product";
import { HomeEverest } from "./home";
declare var require: any;

export const elements: any = {
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: ".btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  btnFinish: 'a[id="cart-to-orderform"]',
  div: ".clearfix.pull-right.cart-links.cart-links-bottom.hide",
  calculeCep: "#shipping-calculate-link",
};

export class Checkout {
  static finish(logged: Boolean = true): void {
    this.inputCep(5,true, false, false);
    cy.get(".btn-success", { timeout: 3000 })
      .contains("Fechar pedido")
      .should("exist")
      .click({ force: true });
    cy.get(elements.div, { timeout: 3000 }).then(() => {
      cy.get(elements.btnFinish, { timeout: 3000 }).click({ force: true });
    });
    cy.wait(2000);
    if (logged) {
      cy.get(elements.inputFirstName).type("Teste",{force:true});
      cy.get(elements.inputLastName).type("Teste",{force:true});
      cy.get(elements.inputCPF).type("565.554.030-47",{force:true});
      cy.get(elements.inputCellPhone).type("11999887766"),{force:true};
      cy.get(elements.btnShipping).click({force:true});
      cy.get(elements.inputNumberHouse).type(
        `${Math.floor(Math.random() * 1)}`,{force:true}
      );
      // cy.get(elements.btnFinishBuy).click({ force: true });
    } else {
      cy.get(elements.fieldEmail).type("teste@teste.com.br");
      cy.get(elements.btnSendMail).click();
      cy.get(elements.btnContinueShoop).click();
    }
  }

  static inputCep(retries?:any, a?: boolean, b?: boolean, c?: boolean) {
    const fakerBr = require("../../../../node_modules/faker-br");
    let cep = fakerBr.address.zipCodeValid();
    cy.get("body").then((body) => {
      cy.wait(2000);
    
      if (retries === 0) {
        throw "Não foi possível encontrar CEP disponível";
      }

      cy.get(".checkout-container.row-fluid.cart-active", {timeout:3000}).then(($els)=>{
        const win = $els[0].ownerDocument.defaultView;
        if (win) win.getComputedStyle($els[0], "before");
        let cepValue = body.find('td[class="info"][style]').length;
        cy.log(String(cepValue));
        if (cepValue === 0) {
          c
            ? cy.get("#cart-reset-postal-code").first().click({ force: true })
            : null;
          a
            ? cy.get(elements.calculeCep).first().click({ force: true })
            : null;
          cy.get(elements.inputCep).clear({force:true});
          cy.get(elements.inputCep).type('20550-162' , {force:true});
          b
            ? cy
                .get("#cart-shipping-calculate")
                .first()
                .click({ force: true })
            : null;
            cy.wait(2000);
            cy.get("#cartLoadedDiv > .cart > table").then((before) => {
              let unavailable = before.find(
                'tbody > tr[class="item-unavailable"] > .item-unavailable-message'
              ).length; 
              if (unavailable > 0) {
                this.inputCep(retries-1,false, false, true); // Melhorar
              }
            });
        }
      })
    });
  }

  static unavaible() {
    cy.get("body").then((body) => {
      let unavaible = body.find('td[class="item-unavailable-message"]').length;
      if (unavaible > 0) {
        cy.get(".item-link-remove").click({ force: true });
        cy.wait(1000);
        cy.get("#cart-choose-products").click({ force: true });
        HomeEverest.accessCategoryAndSubCategory();
        Product.selectProductRandom();
        Product.goCartOrKeepBuying();
        this.finish();
      }
    });
  }
}
