import { HomeEverest } from "./home";
var voltsTimes = 1;

export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-img__img > img",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: 'a[class="buy-button buy-button-ref"]',
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
};

export class Product {
  static selectProductRandom(): void {
    cy.get(elements.imageProduct)
      .its("length")
      .then((length) => {
        cy.get(elements.imageProduct)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
        cy.wait(3000);
      });
  }

  static selectVolts(res: any, retries?: any) {
    let volts = res.find(".x-voltagem").length;

    console.log(volts);

    if (retries === 0) {
      throw "Nao foi possivel encontrar produto disponivel";
    }

    if (volts) {
      console.log("A", voltsTimes);

      cy.get(".x-voltagem > span")
        .its("length")
        .then(() => {
          cy.get(".x-voltagem > span").eq(1).click({ force: true });
        });

      voltsTimes - 1;

      console.log("B", voltsTimes);
    } else {
      HomeEverest.accessCategoryAndSubCategory();
      this.selectProductRandom();
    }
  }

  static goCartOrKeepBuying(retries?: any): void {
    cy.get("body").then((res) => {
      if (retries === 0) {
        throw "Não foi possível encontrar nenhum produto disponivel após 5 tentativas !!";
      }

      let warnMe = res.find(
        '.portal-notify-me-ref > .notifyme.sku-notifyme[style="display: block;"]'
      ).length;

      console.log("WARN", warnMe);

      if (warnMe === 1) {
        this.selectVolts(res, voltsTimes);
        this.goCartOrKeepBuying(retries - 1);
      } else {
        if (res.find(".j-empty-search__tips").length > 0) {
          this.selectProductRandom();
          this.goCartOrKeepBuying(retries);
        } else {
          let button = res.find(
            `.buy-button[style="display: none;"]`
          ).length;

          console.log("button", button);
        
        
          if (button === 1) {
            HomeEverest.accessCategoryAndSubCategory();
            this.selectProductRandom();
            this.goCartOrKeepBuying(retries - 1);
          } else {
       
            cy.wait(3000)

            if(retries === 5) {
           
              cy.get(`${elements.btnBuyProduct}[style="display:block"]`, { timeout: 3000 }).contains('Comprar').should("exist");
              cy.wait(4000);
              cy.get(`${elements.btnBuyProduct}[style="display:block"]`, { timeout: 3000 }).contains('Comprar')
              .invoke("removeAttr", "target")
              .click({ force: true });
            }else {
              
              cy.get(`${elements.btnBuyProduct}[style="display: block;"]`, { timeout: 3000 }).contains('Comprar').should("exist");
              cy.wait(4000);
              cy.get(`${elements.btnBuyProduct}[style="display: block;"]`, { timeout: 3000 }).contains('Comprar')
                .invoke("removeAttr", "target")
                .click({ force: true });
            }
          }
        }
      }
    });
  }

  // console.log(warnMe)
}
