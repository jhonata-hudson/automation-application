export const elements: any = {
  logo: ".j-logo > a",
  btnMyAcount: ".j-popup__link > li >  a",
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  linkCategories: "nav > ul > li",
  linkCategoriesExact: ".j-benefits__right > ul > li > a",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  cookies: ".j-cookieAlert > button",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
  cart: ".j-header__right-link",
  empty: "#cartLoadedDiv",
  removeProduct: ".item-link-remove",
  cartEmpty:
    'div[class="empty-cart-content"][style="display: block;"] > .clearfix.empty-cart-links > #cart-choose-products',
  chooseProduct: "#cart-choose-products",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class HomeEverest {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnMyAcount).contains("Minha Conta").click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.btnMyAcount).contains("Minha Conta").click({ force: true });
    cy.get(elements.btnLoginEmail)
      .contains("Entrar com email e senha")
      .click({ force: true });
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });
    cy.get("body").then((body) => {
      cy.wait(5000);
      cy.url().then((url) => {
        if (
          url !==
          "https://www.softpurificadores.com.br/_secure/account#/profile"
        ) {
          this.validNegative();
        } else if (
          url ===
          "https://www.softpurificadores.com.br/_secure/account#/profile"
        ) {
          if (pass === "invalidPass" || email === "invalidEmail") {
            throw new Error(
              "FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!"
            );
          }

          cy.get(elements.logo).click({ force: true });
        }
      });
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static homeLogo() {
    cy.get(elements.logo).should("exist");
  }

  static clearCart() {
    cy.get("body").then((body) => {
      cy.get(elements.cart).contains("Carrinho").click({ force: true });
      cy.get(".checkout-container.row-fluid.cart-active").then(($els) => {
        const win = $els[0].ownerDocument.defaultView;
        if (win) win.getComputedStyle($els[0], "before");
        cy.get(elements.empty).then((res) => {
          let amount = res.find(elements.cartEmpty).length;
          cy.log(String(amount));
          if (amount === 0) {
            cy.get(elements.removeProduct).click({ force: true });
          }
          cy.get(elements.chooseProduct).click({ force: true });
        });
      });
    });
  }

  static closeSpan() {
    cy.get(elements.spanWelcome).click({ timeout: 30000 });
  }

  static acceptCookie() {
    cy.get(elements.cookies)
      .contains("Entendi e Fechar")
      .click({ timeout: 30000 });
  }

  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let float = Math.random() * length;
        let idx = Math.floor(float);
        cy.get(elements.linkCategoriesExact)
          .eq(idx)
          .click({ force: true });
      });
  }
}
