import { Commons } from "./../commons";
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  dataPerson: 'h1[id="orderform-title"][style="display: block;"]',
  btnContinueShoop: "#btn-identified-user-button",
  calculeCep: "#shipping-calculate-link"


};

export class Checkout {
  static finish(logged: Boolean = true): void {
    Commons.inputCepAleatory(5,elements.inputCep,elements.calculeCep,true);
    cy.get(elements.btnFinish, { timeout: 3000 }).click({force:true});
    cy.get("body").then((body) => {
      if (logged && body.find(elements.dataPerson).length > 0) {
        Commons.inputDataName(
          [elements.inputFirstName,
          elements.inputLastName,
          elements.inputCPF,
          elements.inputCellPhone]
        );
      } else if (!logged && body.find(elements.dataPerson).length === 0) {
        cy.get(elements.fieldEmail).type("teste@teste.com.br", {force:true});
        cy.get(elements.btnSendMail).click({force:true});
        cy.get(elements.btnContinueShoop).click({force:true});
      } else {
        cy.get(elements.fieldEmail).type("teste@teste.com.br", {force:true});
        cy.get(elements.btnSendMail).click({force:true});
        cy.get(elements.btnContinueShoop).click({force:true});
      }
    });
  }
}
