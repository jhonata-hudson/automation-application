export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-buy--cta",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".popup-compra--info__btn > a",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
};

export class Product {
  static selectProductCategory(random: boolean = true, product?: string): void {
    if (random) {
      cy.get("body").then((doc) => {
        if (doc.find(elements.subCategory).length > 0) {
          cy.get(elements.subCategory)
            .its("length")
            .then((length) => {
              cy.get(elements.subCategory)
                .eq(Math.floor(Math.random() * length))
                .click({ force: true });
            });
        } else if (doc.find(elements.fieldEmailBlack).length > 0) {
          cy.get(elements.fieldEmailBlack).type("teste@teste.com.br");
          cy.get(elements.btnNewsletter)
            .click()
            .contains("Enviado")
            .should("exist");
        }
        this.selectProductRandomImage(`${elements.imageProduct}`);
      });
    } else {
      cy.get("a").contains(`${product!}`).click({ force: true });
      cy.get(".fn").contains(`${product!}`).should("exist");
    }
  }

  static selectProductRandomImage(element?: String): void {
    cy.get(`${element!}`).should("exist");

    cy.get(`${element!}`)
      .its("length")
      .then((length) => {
        cy.get(`${element!}`)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
      });
  }

  static goCartOrKeepBuying(): void {
  
    cy.get(elements.btnBuyProduct)
      .contains("Ir para o carrinho")
      .click({ force: true });
  }
}
