import { Commons } from './../commons';
export const elements: any = {
  logo: "a > #Camada_1",
  btnMyAcount: ".j-user-login > a",
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  linkCategories: "nav > ul > li",
  linkCategoriesExact: "nav > ul > li > a",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  cart: ".j-minicart--icon",
  removeItens: 'small[class="j-minicart-remove__item"]',
  contentCart: ".j-minicart-items__list > li",
  removeProduct: ".j-minicart-remove__item",
  cartEmpty: ".j-minicart-empty",
  closeCart: ".j-minicart-close.js--close-minicart",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class HomeSoftys {
  static validCode(invalid?: String) {
  
    let inboxId;
    
    cy.get(elements.btnMyAcount)
      .contains("Olá. Faça seu login")
      .click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.btnMyAcount)
      .contains("Olá. Faça seu login")
      .click({ force: true });
    cy.get(elements.btnLoginEmail)
      .contains("Entrar com email e senha")
      .click({ force: true });
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });
    cy.get("body").then((body) => {
      cy.wait(5000);
      cy.url().then((url) => {
        if (url !== "https://www.lojasoftys.com.br/_secure/account#/profile") {
          this.validNegative();
        } else if (
          url === "https://www.lojasoftys.com.br/_secure/account#/profile"
        ) {
          if (pass === "invalidPass" || email === "invalidEmail") {
            throw new Error(
              "FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!"
            );
          }
          cy.get(elements.loginOk).should("have.text", "Olá");
          cy.get(elements.logo).click({ force: true });
        }
      });
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }
  static homeLogo() {
    cy.get(elements.logo).should("exist");
  }

  static closeSpan() {
    cy.get(elements.spanWelcome).click({ timeout: 30000 });
  }

  static acceptCookie() {
    cy.get(elements.cookies).click({ timeout: 30000 });
  }

  static clearCart() {
    cy.get(elements.cart).click({ force: true });
    cy.get("body").then((res) => {
      let amount = res.find(elements.contentCart).length;
      if (amount >= 1) {
        cy.get(elements.removeProduct).click({ force: true });
        cy.get(elements.cartEmpty).should("exist");
      }
    });
    cy.get(elements.closeCart).click({ force: true });
  }
  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let float = Math.random() * length;
        let idx = Math.floor(float);
        if (idx != 4) {
          cy.get(elements.linkCategoriesExact).eq(idx).click({ force: true });
        } else {
          cy.get(elements.linkCategories).eq(idx).find("div").invoke("show");
          cy.get(elements.subCategory)
            .its("length")
            .then((length) => {
              cy.get(elements.subCategory)
                .eq(Math.floor(Math.random() * length))
                .click({ force: true });
            });
        }
      });
  }
}
