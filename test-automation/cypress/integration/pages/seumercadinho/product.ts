import { HomeMercadinho } from "../seumercadinho/home";

export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-img__img > img",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".popup-compra--info__btn > a",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  productItem: ".j-shelf__item-buy--cta",
};

export class Product {
  static selectProduct(): void {
    cy.get("body").then((body) => {
      cy.wait(3000);
      let length = body.find(elements.productItem).length;
      if (length === 0 && body.find(".outOfStock").length > 0) {
        cy.go("back");
        HomeMercadinho.accessCategoryAndSubCategory();
        this.selectProduct();
      } else {
        cy.get(elements.productItem)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
        cy.wait(4000);
      }
    });
  }

  static goCartOrKeepBuying(): void {
  
    cy.get(".d-dep-cat.search").then((body) => {
      let modal = body.find(".popup-compra.y-close").length;
      if (modal === 0) {
        cy.reload();
        this.selectProduct();
        this.goCartOrKeepBuying();
      }
      cy.get(elements.btnBuyProduct, { timeout: 3000 })
        .contains("Ir para o carrinho")
        .invoke("removeAttr", "target")
        .click({ force: true });
    });

    // cy.get(".buy-button.buy-button-ref", { timeout: 3000 })
    //   .contains("Comprar")
    //   .invoke("removeAttr", "target")
    //   .click({ force: true });
  }
}
