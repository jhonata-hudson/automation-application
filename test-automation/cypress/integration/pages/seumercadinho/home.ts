export const elements: any = {
  logo: "a > #Camada_1",
  btnMyAcount: ".login",
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  filterCategories: ".search-single-navigator > ul > li",
  linkCategoriesExact: ".j-header__menu--aditional > li",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  cookies: ".j-cookieAlert > button",
  fieldBlack: ".cl_email.email",
  acceptBlack: ".newsletter-button-ok",
  cart: 'span[class="j-minicart--icon"]',
  contentCart: ".j-minicart-items__list > li",
  removeProduct: ".j-minicart-remove__item > img",
  cartEmpty: ".j-minicart-empty",
  closeCart: ".j-minicart-close.js--close-minicart",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class HomeMercadinho {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnMyAcount).contains("Entrar").click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.btnMyAcount).contains("Entrar").click({ force: true });
    cy.get(elements.btnloginEmailAndPassword).click({ force: true });
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });
    cy.wait(2000);
    cy.get("body").then((url) => {
      let ola = url.find(elements.alert).length;
      cy.log(String(ola));
      if (ola >= 1) {
        this.validNegative();
      } else if (ola === 0) {
        if (pass === "invalidPass" || email === "invalidEmail") {
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!");
        }
      }
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static clearCart() {
    cy.get(elements.cart).click({ force: true });
    cy.get("body").then((res) => {
      let amount = res.find(elements.contentCart).length;
      if (amount >= 1) {
        cy.get(elements.removeProduct).click({ force: true });
        cy.get(elements.cartEmpty).should("exist");
      }
    });
    cy.get(elements.closeCart).click({ force: true });
  }

  static acceptCookie() {
    cy.get(elements.cookies, { timeout: 3000 }).click({ force: true });
  }

  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let float = Math.random() * length;
        let idx = Math.floor(float);
        cy.get(`${elements.linkCategoriesExact} > a`)
          .eq(idx)
          .click({ force: true });

        cy.wait(1000);
        cy.get(elements.filterCategories, { timeout: 3000 })
          .its("length")
          .then((amount) => {
            cy.get(`${elements.filterCategories} > a`)
              .eq(Math.floor(Math.random() * amount))
              .click({ force: true });
          });
      });
  }
}
