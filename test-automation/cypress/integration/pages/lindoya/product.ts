import { HomeLindoya } from "../lindoya/home";

export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".product-item__top-box--productImage",
  fieldEmailBlack: "#email",
  btnNewsletter: "button",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  filterAllCategorys:
    ".j-category__searchNavigator > .navigation > div > div > h3 > a",
  filterSubCategorys: ".j-category__searchNavigator > .navigation > div > div",
  filterSubCategorysTxt:
    ".j-category__searchNavigator > .navigation > div > .search-single-navigator > h3",
  classDrinks: ".j-drinks__nav > div > ul > li ",
  product: ".vitrine.prateleira.js--shelf > ul > li",
  drinksTypes: ".j-drinks__list",
  btnNextImg: ".slick-next.slick-arrow",
  ytb: ".receitas > .j-drinks__middle--video > iframe",
  btnFastBuy: ".j-shelf__item-actions-add-cart",
  btnBuyCart: ".j-buy > a",
  ofStockProduct: 'p[class="outOfStock"]',
  btnBuyInCart: "#cart-to-orderform",
  btnKeepBuy: ".link-choose-more-products-wrapper",
  btnBuyProducts: ".popup-compra--info__btn > a",

};

export class Product {
  static selectProductRandom(): void {
    cy.get("body").then((body) => {
      let lengthOfStok = body.find(elements.ofStockProduct).length;
      cy.get(elements.product)
        .its("length")
        .then((lengthAll) => {
          let index = lengthAll - lengthOfStok;
          let indexFinish = Math.floor(Math.random() * index);
          cy.get(elements.imageProduct).eq(indexFinish).click({ force: true });
        });
    });
  }

  static selectSizeAndQuantity(): void {
    cy.get("body", { timeout: 3000 }).then((body) => {
      cy.get(".qtd").clear();
      let idx = Math.floor(Math.random() * 50);
      cy.get(".qtd").type(`${idx.toString()}`);
    });
  }

  static goCartOrKeepBuying(): void {
    cy.get('body > main').then((body)=>{
      let unavailable = body.find('input[class="sku-notifyme-client-name notifyme-client-name"][style="display: block;"]').length
      if (unavailable > 0){
        HomeLindoya.accessCategoryAndSubCategory()
        this.selectProductRandom()
        this.goCartOrKeepBuying()
      } else {
        cy.get(elements.btnBuyCart).invoke("removeAttr", "target").click({force:true});
        cy.get(elements.btnBuyProducts)
        .contains("Ir para o carrinho")
        .invoke("removeAttr", "target")
        .click({ force: true });

      }
    })
  }
}
