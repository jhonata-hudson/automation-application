declare var require: any;

const { MailSlurp } = require("mailslurp-client");
const apiKey =
  "5b4f385452287a694109a8635e9de8b97234f9f4b6f683eae8c7b623b27938f4";

const mailslurp = new MailSlurp({ apiKey });

export function sendDataMongoDb(
  store: string,
  errMsg: string,
  date: string,
  hour: string
) {
  const body = {
    store: store,
    errorMessage: errMsg,
    date: date,
    hour: hour,
  };

  cy.request({
    method: "POST",
    url: "https://dr835m9fu0.execute-api.us-east-1.amazonaws.com/api/messaging",
    body: body,
    timeout: 50000,
  }).then((response) => {
    new Promise(function () {
      console.log("sucesso");
      expect(response.status).to.eq(200);
    });
  });

  console.log("batatata");
}

export class SendMail {
  static waitForLatestEmail(inboxId: string) {
    return mailslurp.waitForLatestEmail(inboxId);
  }

  static getInbox(inboxId: string) {
    return mailslurp.waitForLatestEmail(inboxId);
  }

  static createInbox() {
    // instantiate MailSlurp
    const mailslurp = new MailSlurp({ apiKey });
    // return { id, emailAddress } or a new randomly generated inbox
    return mailslurp.createInbox();
  }
}
