import { Product } from './product';
import { HomeTupperware } from './home';
import { Commons } from "./../commons";
import { CheckoutPayot } from "../../pages/payot/checkout";

declare var require: any;
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  calculeCep: ".custom-cep > .form-calculate > #btn-simulate",
};

export class Checkout {
  static cepTupper(retries: any, retriesField?: any, logged?: string) {
    const fakerBr = require("../../../../node_modules/faker-br");
    let cep = fakerBr.address.zipCodeValid();
    
    cy.get("body").then((body) => {
      if (retries === 0) {
        throw "Não foi possível encontrar CEP disponível";
      }
      if (retriesField === 0) {
        throw "Não foi possível encontrar CEP disponível";
      }
      let fieldCep = body.find(elements.inputCep).length

      cy.get(elements.inputCep).type(cep, { force: true });
      cy.wait(2000);
      cy.get(".row-fluid.orderform-template.span12.active").then(($els) => {
        const win = $els[0].ownerDocument.defaultView;
        if (win) win.getComputedStyle($els[0], "before");
        cy.get(".hproduct.item.muted").then((before) => {
          let unavailable = before.find(
            'div[class="item-unavailable"] > .item-unavailable-message'
          ).length;
          cy.log(String(unavailable));
          if (unavailable > 0) {
            this.cepTupper(5); // Melhorar
          }
        });
      });
    });
  }

  static finish(logged:string): void {
    //this.cepTupper();
    cy.get(elements.btnFinish, { timeout: 3000 }).click({force:true});
    if (logged === 'false') {
      cy.get(elements.fieldEmail).type("teste@teste.com.br", { force: true });
      cy.get(elements.btnSendMail).click({ force: true });
      this.loggedCheck(logged);
    } else {
      cy.get(elements.fieldEmail).type("teste@teste.com.br", { force: true });
      cy.get(elements.btnSendMail).click({ force: true });
      cy.get(elements.inputFirstName, { timeout: 3000 }).type("Teste", {
        timeout: 3000,
      });
      cy.get(elements.inputLastName, { timeout: 3000 }).type("Teste", {
        timeout: 3000,
      });
      cy.get(elements.inputCPF).type("565.554.030-47");
      cy.get(elements.inputCellPhone).type("11999887766");

    }
  }

  static loggedCheck(logged?:string) {
    cy.wait(3500);
    const fakerBr = require("../../../../node_modules/faker-br");
    let cep = fakerBr.address.zipCodeValid();
    cy.get(elements.inputFirstName, { timeout: 3000 }).type("Teste", {
      timeout: 3000,
    });
    cy.get(elements.inputLastName, { timeout: 3000 }).type("Teste", {
      timeout: 3000,
    });
    cy.get(elements.inputCPF).type("565.554.030-47");
    cy.get(elements.inputCellPhone).type("11999887766");
    cy.get(elements.btnShipping).dblclick({ force: true });
   
    this.cepTupper(5,1,logged)

    cy.get(elements.inputNumberHouse).type(
      `${Math.floor(Math.random() * 1000)}`,
      { force: true }
    );
    cy.get(elements.btnFinishBuy).click({ force: true });
  }
}
