import { HomeTupperware } from './home';
import { Checkout } from './checkout';
export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-img__img > img",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  productItem: ".j-shelf__item-buy",
  btnFastBuy: ".j-shelf__item--bottom",
  btnBuyCart: ".j-minicart-btn",
};

export class Product {
  static selectProductCategory(): void {
    cy.get(elements.btnFastBuy)
      .its("length")
      .then((length) => {
        let ammountProduct = Math.floor(Math.random() * length);
        cy.get(elements.btnFastBuy)
          .eq(ammountProduct)
          .find("a")
          .first()
          .click({ force: true });
      });
  }

  static goCart(): void {
    cy.get(elements.btnBuyCart).click({ force: true });
  }
}
