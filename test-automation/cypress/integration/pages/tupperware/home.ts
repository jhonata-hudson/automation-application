export const elements: any = {
  logo: "#Logo_Tupperware",
  btnMyAcount: ".j-user-login > a",
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  linkCategories: ".search-single-navigator > h3",
  linkCategoriesExact: "nav > ul > li > a",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  fieldModal: 'input[type="email"]',
  modalEmail: ".welcome-form",
  buttonSubmit: ".welcome-submit",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  cookies: ".j-cookies-button > button",
  authenticate: ".vtex-account__user-greeting",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class HomeTupperware {
  static selectVisits(url?: String) {
    switch (url) {
      case "lojaVirtual":
        this.visitPage(
          "https://www.lojavirtualtupperware.com.br/produtos?_ga=2.64431787.504046673.1605548105-1631269416.1605548105"
        );
        break;
      case "login":
        this.visitPage(
          "https://www.lojavirtualtupperware.com.br/login?ReturnUrl=%2f_secure%2faccount"
        );
        break;
      default:
        this.visitPage("https://www.tupperware.com.br/");
    }
  }

  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static visitPage(site: string) {
    cy.visit(site);
  }

  static homeLogo() {
    cy.get(elements.logo).should("exist");
  }

  static closeSpan() {
    cy.get(elements.spanWelcome).click({ timeout: 30000 });
  }

  static acceptCookie() {
    cy.get(elements.cookies)
      .eq(1)
      .click({force:true});
  }

  static modalEmail() {
    cy.get(elements.modalEmail)
      .find(elements.fieldModal)
      .type("teste@teste.com.br");
    cy.get(elements.buttonSubmit).click();
    cy.wait(1000);
  }

  static loginSite(pass: string, email: string) {
    cy.get(elements.btnloginEmailAndPassword).click();

    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click();
    cy.get("body").then((body) => {
      cy.wait(5000);
      cy.url().then((url) => {
        if (
          url !==
          "https://www.lojavirtualtupperware.com.br/_secure/account#/profile"
        ) {
          this.validNegative();
        } else if (
          url ===
          "https://www.lojavirtualtupperware.com.br/_secure/account#/profile"
        ) {
          if (pass === "invalidPass" || email === "invalidEmail") {
            throw new Error(
              "FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!"
            );
          }
        }
      });
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static accessCategoryAndSubCategory(logged?: boolean): void {
    cy.wait(2000);
    !logged
      ? cy
          .get(elements.linkCategoriesExact, { timeout: 3000 })
          .first()
          .click({ force: true })
      : cy
          .get(elements.linkCategories, { timeout: 3000 })
          .first()
          .click({ force: true });

    cy.get(elements.linkCategories, { timeout: 3000 })
      .its("length")
      .then((length) => {
        let index = Math.floor(Math.random() * length);
        cy.get(`${elements.linkCategories} > a`, { timeout: 3000 })
          .eq(index)
          .click();
      });
  }
}
