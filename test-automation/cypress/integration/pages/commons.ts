import { HomeUsaflex } from "./usaflex/home";
declare var require: any;
import { ProductAmericanPet } from "../pages/americanpet/product";
let useCep: any
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  destiny: "#ship-receiverName",
};

export class Commons {
  static validCep(cep: string, element?: String) {
    if (cep === "buscaCep") {
      cy.get(elements.btnFinish).click({ force: true });
      element ? this.inputEmail(`${element}`) : null;
      cy.wait(4000);
      cy.get(elements.inputFirstName, { timeout: 3000 }).type("Teste", {
        force: true,
      });
      cy.get(elements.inputLastName, { timeout: 3000 }).type("Teste", {
        force: true,
      });
      cy.get(elements.inputCPF, { timeout: 3000 }).type("565.554.030-47", {
        force: true,
      });
      cy.get(elements.inputCellPhone, { timeout: 3000 }).type("11999887766", {
        force: true,
      });
      cy.get(elements.btnShipping).click();
      cy.get(".input > small > a")
        .invoke("removeAttr", "target")
        .click({ force: true });
      cy.url({ timeout: 3000 }).should(
        "include",
        "http://www.buscacep.correios.com.br/"
      );
    } else if (cep === "InvalidCep") {
      cy.get(elements.inputCep).type("00000000");
    }
  }

  static inputDataName(data: Array<string>, clear?: boolean) {
    cy.log(String(data));
    let dataFields = ["teste", "teste", "565.554.030-47", "11968775283"];
   
    cy.log('INICIANDO INPUT DE DADOS COMO NOME E CPF');

   
    for (let index = 0; index < data.length; index++) {
      if (clear) {
       
        
        cy.log(`CAMPO - ${index} > ${data[index]}`);

       
        cy.get(data[index], { timeout: 2000 })
          .should("exist")
          .clear({ force: true });
      }
      cy.get(data[index], { timeout: 2000 })
        .should("exist")
        .type(dataFields[index], {
          force: true,
        });
        cy.log(`CAMPO - ${index} > ${data[index]}, PREENCHIDO COM SUCESSO`);
      }


    cy.get(elements.btnShipping).should("exist").click({ force: true });

    cy.log(`CLIQUEI EM CONFIRMAR DADOS INSERIDOS COM SUCESSO -----`);

    cy.get("body").then((body) => {
      
      cy.log(`INICIANDO WAIT DE 2s-----`);

      cy.wait(2000);
      let shipNumber = body.find(elements.inputNumberHouse).length;
      if (shipNumber === 0) {
        this.inputDataName(data, true);
      } else {
        cy.get(elements.inputNumberHouse)
          .should("exist")
          .type(`${Math.floor(Math.random() * 1)}`);
        
        
        cy.log(`CAMPO NUMERO DE CASA INSERIDO COM SUCESSO-----`);


        cy.get(elements.destiny, { timeout: 2000 })
          .should("exist")
          .type("Teste", {
            force: true,
          });

        cy.log(`CAMPO DESTINO INSERIDO COM SUCESSO-----`);

        cy.get(elements.btnFinishBuy, { timeout: 2000 })
          .should("exist")
          .click({ force: true });
        
         
        cy.log(`DADOS DE CHECKOUT OK -----`);

      }
    });
  }

  static inputEmail(element?: string) {
    cy.get(`${element}`).type("teste@teste.com.br");
    cy.get("#btn-client-pre-email").click({ force: true });
  }

  static deleteCart(element: string, empty: string, store?: string) {
    cy.get("body")
      .should("exist")
      .then((body) => {
        cy.wait(2000);
        let remove = body.find(`${element}`).length;
        if (remove > 0) {
          for (let index = 0; index < remove; index++) {
            cy.get(`${element}`).should("exist").first().click({ force: true });
          }
        } else if (remove === 0) {
          cy.go("back");
          this.addAgainProduct(store!);
          this.deleteCart(element, empty, store);
        }
        cy.get(`${empty}`).should("exist");
      });
  }

  static addAgainProduct(store: string) {
    switch (store) {
      case "AmericanPet":
        ProductAmericanPet.selectProduct();
        ProductAmericanPet.goCartOrKeepBuying();
        break;
    }
  }

  static inputCepAleatory(
    retries: number,
    fieldCep?: string,
    calcule?: string,
    click?: boolean,
    ceps?: string

  ) {
    
    cy.log('VOU ESCOLHER UM CEP ----------')


    const fakerBr = require("../../../node_modules/faker-br");
   
    if (ceps != undefined){
      useCep = ceps
    } else {
      useCep = fakerBr.address.zipCodeValid();
    }

    cy.get("body").then((body) => {
      cy.log(String(retries));

      cy.wait(2000);

      if (retries === 0) {
        throw "Não foi possível encontrar CEP disponível";
      }
      

      cy.get(".checkout-container.row-fluid.cart-active").then(($els) => {
        const win = $els[0].ownerDocument.defaultView;
        if (win) win.getComputedStyle($els[0], "before");
        let cepFull = body.find(".srp-address-title.link.pointer.blue.f5")
          .length;
        cy.log(String(cepFull));

        if (cepFull === 0) {
         
          click
            ? cy
                .get(calcule!, { timeout: 3000 })
                .should("exist").click({force:true})
                
            : null;
          
          cy.log('CLIQUEI EM CALCULAR CEP ----------')

          cy.wait(2000)
          cy.get(fieldCep!, { timeout: 3000 })
            .should("exist")
            .clear({ force: true });

          cy.log('CLEAR CEP  ----------')

          cy.get(fieldCep!, { timeout: 3000 }).should("exist").type(useCep, {
            force: true,
          });
         
          cy.log('INPUT CEP  ----------')

          cy.wait(5000);
          cy.get("#cartLoadedDiv > .cart > table", { timeout: 3000 })
            .should("exist")
            .then((before) => {
              let unavailable = before.find(
                'tbody > tr[class="item-unavailable"]'
              ).length;

              cy.log('CHECANDO INDISPONIBILIDADE  ----------')


              if (unavailable > 0) {
               cy.log('DEU INDISPONIBILIDADE  ----------')
                
                return this.inputCepAleatory(
                  retries - 1,
                  fieldCep,
                  calcule,
                  false,
                  ceps
                ); // Melhorar
              }
            });
        }
      });
    });

    cy.log('FECHEI O METODO DE CEP ----------')
  }

  static searchInvalid(
    input?: string,
    button?: string,
    empty?: string,
    store?: string
  ) {
    if (input === "#Capa_1" || input === ".search__input") {
      cy.get(`${input}`).click({ force: true });
      cy.get(`${button}`).type("123456");
      cy.get(`${empty}`).click({ force: true });
      input === "#Capa_1"
        ? cy.get(".j-empty__main").should("exist")
        : cy.get(".rkt-search-results-not-found-step3").should("exist");
    } else {
      store === "UsaFlex"
        ? HomeUsaflex.searchInvalidProduct(input!, button!)
        : cy.get(`${input}`).type("123456");
      cy.get(`${button}`).dblclick({ force: true });
      cy.wait(1000);
      empty ? cy.get(`${empty}`).should("exist") : null;
    }
  }
}
