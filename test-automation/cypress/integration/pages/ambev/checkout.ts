import { Commons } from './../commons';
export const elements: any = {
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postal-code",
  inputNumberHouse: "#ship-number",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  btnFinish: "#cart-to-orderform",
  razaoSocial: "#client-company-name",
  nameFantasy: "#client-company-nickname",
  isentTaxa: "#state-inscription",
  btnFinishBuy: ".btn-go-to-payment-wrapper > .submit",
  calculeCep: "#shipping-calculate-link"
};

export class Checkout {
  static finish(): void {
  
    Commons.inputCepAleatory(elements.inputCep,elements.calculeCep);
 
    cy.get(elements.btnFinish, {timeout:3000})
      .click()
      .visit("https://www.franquiaambev.com.br/checkout#/profile");
    cy.get(elements.inputFirstName).type("Teste");
    cy.get(elements.inputLastName).type("Teste");
    cy.get(elements.inputCPF).type("565.554.030-47");
    cy.get(elements.inputCellPhone).type("11999887766");
    cy.get(elements.razaoSocial).type("TESTE LTDA");
    cy.get(elements.nameFantasy).type("EMPRESA TESTE");
    cy.get(elements.isentTaxa).click();
    cy.get(elements.btnShipping).click();
    cy.get(elements.inputNumberHouse).type(`${Math.floor(Math.random() * 1)}`);
    cy.get(elements.btnFinishBuy).click();
  }
}
