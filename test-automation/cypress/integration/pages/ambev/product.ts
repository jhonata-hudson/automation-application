export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".shelf-product__link.shelf-product__link--image > img",
  fieldEmailBlack: "#email-newsletter",
  btnNewsletter: "#btn-newsletter",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".product-buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  productItem: ".shelf-product__link.shelf-product__link--image",
  goCart: ".swal-button.swal-button--success",
};

export class Product {
  static selectProduct(): void {
    cy.get(elements.productItem)
      .its("length")
      .then((length) => {
        cy.get(elements.productItem)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
      });
  }

  static goCartOrKeepBuying(): void {
    cy.wait(1500)
    cy.get(elements.btnBuyProduct).dblclick({ force: true });
    cy.get("body").then(() => {   
      cy.get(elements.goCart).first().dblclick({ force: true });
    });
  }
}
