export const elements: any = {
  logo: "a > #Camada_1",
  btnMyAcount: "#auth-cnpj",
  btnLoginEmail: ".auth-form__button",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  linkCategories: "nav > ul > li",
  linkCategoriesExact: ".search-single-navigator > h3",
  linkSubCategories: "a",
  subCategory: ".auth-form__fieldset > # > li > a",
};

export enum Categories {
  paraSeuNegocio,
  cuidadosPessoais,
  cuidadosComCasa,
  mascarasDeProtecao,
  nossasMarcas,
  blackFriday,
  outlet,
}

export class Home {
  static loginPage(invalid?: boolean) {
  
    if (invalid === true) {
     
      cy.get(elements.btnMyAcount).type("00.000.000/0000-00", { force: true });
      
      
      cy.get(elements.btnLoginEmail).contains("Entrar").click({ force: true });
      cy.get(".auth-form__text.auth-form__text--big")
        .contains("O CNPJ informado não é válido.")
        .should("exist");
    } else {
      cy.get(elements.btnMyAcount).type("68.719.452/0001-61", { force: true });
      cy.get(elements.btnLoginEmail).contains("Entrar").click({ force: true });
    }
  }

  static accessCategoryAndSubCategory() {
   
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let number = Math.random() * length;
        console.log(number)
        let idx = Math.floor(number);
        cy.get(`${elements.linkCategoriesExact} > a`)
          .eq(idx)
          .click({ force: true });
      });
  }
}
