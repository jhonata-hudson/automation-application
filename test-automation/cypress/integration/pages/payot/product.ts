export const elements: any = {
  productItem: ".j-shelf__item-buy",
  btnWishList: ".btn.btn-block.btn-outline-secondary.y-wishBtn.btn-wishlist",
  selectModel: ".group_0",
  selectModelLabel: ".sku-selector-container > ul li span label",
  selectModelSpan: ".sku-selector-container > ul li span",
  btnBuyProduct: ".j-minicart-btn",
  divBuyInfo: ".buy-button.buy-button-ref",
  search: 'input[class="__rh-sb-input__ rh-sb-input"]',
};

export class Product {
  static selectProduct(): void {
    cy.get(elements.productItem)
      .its("length")
      .then((length) => {
        cy.get(elements.productItem)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
      });
  }

  static goCartOrKeepBuying(): void {
    cy.wait(1000);
    cy.get(elements.btnBuyProduct).click({ force: true });
  }
}
