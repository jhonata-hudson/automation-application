export const elements: any = {
  spanWelcome: ".vex-close",
  linkCategories: "nav > ul > li > a",
  linkSubCategories: "a",
  btnLogin: ".j-list-login",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  inputEmail: "#leadEmail",
  inputName: "#leadNome",
  inputEmailLogin: "#inputEmail",
  inputPassLogin: "#inputPassword",
  inputDateNasc: "#leadNascimento",
  inputPhone: "#leadTelefone",
  btnSingIn: "#classicLoginBtn",
  btnSendMail: "#send-btn",
  elCategoryUnavaible: "#busca-vazia-extra-middle",
  cookies: ".j-cookieAlert > button",
  logo: "#Logo_Payot_2020_aprovado",
  marcas: ".j-header__drop--list.is--brands > .j-other > li",
  categorys: ".j-list__departments.is--desktop > li",
  linkCategoriesExact: "nav > ul > li > a",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  cachorro,
  gato,
  pássaro,
  peixe,
  roedores,
  farmácia,
  promo,
  marcas,
}

export const indxCategory: any = {
  0: "cachorro",
  1: "gato",
  2: "pássaro",
  3: "peixe",
  4: "roedores",
  5: "farmácia",
  6: "promo",
  7: "marcas",
};
export class HomePayot {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get("a")
      .contains("Faça seu login")
      .invoke("show")
      .click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static homeLogo() {
    cy.get(elements.logo).should("exist");
  }

  static closeSpan(popup?: boolean) {
    cy.get("body").then((res) => {
    
      cy.wait(4000)
     
      let span =res.find(elements.spanWelcome).length
      if (span === 0 && popup) {
        cy.reload();
        this.modalEmail();
      } else if(popup && span === 1) {
        cy.get(elements.spanWelcome).click({ force: true });
        this.modalEmail()
      } else if (span === 1) {
        cy.get(elements.spanWelcome).click({ force: true })
      }
    });
  }

  static modalEmail() {
    cy.get(elements.inputEmail).type("teste@teste.com.br");
    cy.get(elements.inputName).type("Teste Teste");
    cy.get(elements.inputDateNasc).click();
    cy.get('input[id="leadNascimento"][onfocus]').invoke(
      "attr",
      "onfocus",
      '(this.type="text")'
    );
    cy.get(elements.inputPhone).type("11999998888");
    cy.get(elements.inputDateNasc).click();
    cy.get(elements.inputDateNasc).type("30041997", { force: true });
    cy.get(elements.btnSendMail).click();
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get("a")
      .contains("Faça seu login")
      .invoke("show")
      .click({ force: true });
    cy.get(elements.btnloginEmailAndPassword).click();
    cy.get(elements.inputEmailLogin).type(infos[fieldEmail + 1], {
      force: true,
    });
    cy.get(elements.inputPassLogin).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnSingIn).click();
    cy.wait(3000);
    cy.get("body").then((url) => {
      let ola = url.find(elements.alert).length;
      cy.log(String(ola));
      if (ola >= 1) {
        this.validNegative();
      } else if (ola === 0) {
        if (pass === "invalidPass" || email === "invalidEmail")
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!");
      }
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static acceptCookie() {
    cy.get(elements.cookies)
      .contains("Entendi e Fechar")
      .click({ timeout: 30000 });
  }

  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let float = Math.random() * length;
        let idx = Math.floor(float);
        cy.get(elements.linkCategories).eq(idx).click({ force: true });
      });
  }
}
