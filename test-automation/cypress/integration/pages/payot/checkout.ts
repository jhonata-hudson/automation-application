import { Commons } from './../commons';
import { HomePayot } from "../../pages/payot/home";
declare var require: any;

export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: ".j-minicart-btn",
  btnGoPayment: "#btn-go-to-payment",
  spanWelcome: ".vex-close",
  calculeCep: "#shipping-calculate-link",
  backToCart: '#orderform-to-cart'


};

export class CheckoutPayot {
  static finish(logged: Boolean = true): void {
    HomePayot.closeSpan(false)
    cy.get(elements.backToCart).click({force:true})
    Commons.inputCepAleatory(5,elements.inputCep,elements.calculeCep,true);
    cy.get(elements.btnFinish).click({force:true})
    if (logged) {
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      );
    } else {
      cy.get(elements.inputClientEmail).type("teste@teste.com.br", {
        force: true,
      });
      cy.get(elements.btnClientEmail).click({ force: true });
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      );
    }
  }

}
