import { HomeUsaflex } from "./home";
export const elements: any = {
  productItem: ".y-shelf__item-img__img",
  btnWishList: ".btn.btn-block.btn-outline-secondary.y-wishBtn.btn-wishlist",
  selectModel: ".group_0",
  selectModelLabel: ".sku-selector-container > ul li span label",
  selectModelSpan: ".sku-selector-container > ul li span",
  btnBuyProduct: ".buy-button.buy-button-ref",
  divBuyInfo: ".popup-compra--info__btn",
  search: 'input[class="__rh-sb-input__ rh-sb-input"]',
  btnBuyCart: ".y-buy > a",
};

export class Product {
  static selectProduct(retries?: any): void {
    cy.get("body").then((body) => {
      let itens = body.find(elements.productItem).length;

      console.log(itens);

      cy.wait(2000);

      if (retries === 0) {
        throw "Não foi possivel visualizar produtos !! ";
      }

      if (itens === 0) {
        HomeUsaflex.accessCategoryAndSubCategory();
        this.selectProduct(retries - 1);
      } else {
        cy.get(elements.productItem)
          .its("length")
          .then((length) => {
            cy.get(elements.productItem)
              .eq(Math.floor(Math.random() * length))
              .click({ force: true });
          });
      }
    });
  }

  static wishList(): void {
    cy.get(elements.btnWishList).click();
  }

  static selectModelAndBuy(): void {
    cy.get("body").then((body) => {
      if (body.find(`${elements.selectModel} > label`).length > 1) {
        cy.get(`${elements.selectModel} > label`)
          .its("length")
          .then((totalSizes) => {
            cy.get(elements.selectModel).then((idx) => {
              if (idx.find('label[class$="disabled"]').length > 0) {
                let allSizesUnavaible = idx.find('label[class$="disabled"]')
                  .length;
                let sizesUnavaible = [];
                let sizesAvaible = [];
                for (let index = 0; index < allSizesUnavaible; index++) {
                  sizesUnavaible[index] = idx
                    .find('label[class$="disabled"]')
                    .eq(index)
                    .text();
                }
                for (let i = 0; i < totalSizes; i++) {
                  sizesAvaible[i] = idx.find("label").eq(i).text();
                }
                let toRemove = new Set(sizesUnavaible);
                let sizesAvaibles = sizesAvaible.filter(
                  (x) => !toRemove.has(x)
                );
                sizesAvaibles.length > 1
                  ? cy
                      .get("label")
                      .contains(
                        sizesAvaibles[
                          Math.floor(Math.random() * sizesAvaibles.length)
                        ]
                      )
                      .click({ force: true })
                  : cy
                      .get("label")
                      .contains(`${sizesAvaibles[0]}`)
                      .click({ force: true });
              } else {
                idx
                  .find("label")
                  .eq(Math.floor(Math.random() * length))
                  .click();
              }
            });
          });
      }
    });
  }

  static goCartOrKeepBuying(): void {
    cy.get(elements.btnBuyCart)
      .should("exist")
      .invoke("removeAttr", "target")
      .dblclick({ force: true });
  }
}
