import { Commons } from "./../commons";
declare var require: any;
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: ".btn-go-to-payment",
  calculeCep: "#shipping-calculate-link",
};

export class Checkout {
  static finish(logged: Boolean = true): void {
  //  Commons.inputCepAleatory(elements.inputCep, elements.calculeCep, true);
  //  this.inputCep(false);
    cy.wait(2000);
    cy.get(elements.btnFinish, { timeout: 3000 }).click();
    if (logged) {
      this.loggedCheck();
    } else {
      cy.get(elements.fieldEmail).type("teste@teste.com.br", { force: true });
      cy.get(elements.btnSendMail).click();
      cy.get(elements.btnContinueShopping).click();
    }
  }

  static loggedCheck() {
    cy.wait(2000);
    cy.get(elements.inputFirstName, { timeout: 3000 }).type("Teste", {
      force:true,
    });
    cy.get(elements.inputLastName, { timeout: 3000 }).type("Teste", {
      force:true,
    });
    cy.get(elements.inputCPF).type("565.554.030-47", {
      force:true,
    });
    cy.get(elements.inputCellPhone).type("11999887766", {
      force:true,
    });
    cy.get(elements.btnShipping).click( {
      force:true,
    });
    cy.get(elements.inputCep, { timeout: 3000 }).type('03512020', {
      force: true,
    });
  
    cy.get(elements.inputNumberHouse).type(
      `${Math.floor(Math.random() * 1000)}`, {
        force:true,
      }
    );
    cy.get(elements.btnFinishBuy).click();
  }
}
