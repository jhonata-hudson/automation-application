export const elements: any = {
  spanWelcome: "#js--close",
  linkCategories: ".y-menu__departamento > li",
  linkSubCategories: "a",
  btnLogin: "#classicLoginBtn",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnSingIn: "#classicLoginBtn",
  elCategoryUnavaible: "#busca-vazia-extra-middle",
  cookies: ".j-cookieAlert > button",
  modalEmail: "#roulette-email",
  modalName: "#roulette-name",
  radioAccept: "#roulette-accept",
  backSite: ".back-to-site",
  loginOk: ".vtex-account__user-greeting.f4.fw3 > span",
  logo: ".y-header__top--left > a",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  feminino,
  masculino,
  juvenil,
  infantil,
  calcados,
  acessorios,
  relogios,
  casa,
  beleza,
  eletronicos,
  marcas,
  ofertas,
}

export class HomeUsaflex {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(".y-header__account-subs >  li > a")
      .invoke("show")
      .contains("Minha Conta")
      .click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }
  static login(pass: string, email: string) {
    cy.get(".y-header__account-subs >  li > a")
      .invoke("show")
      .contains("Minha Conta")
      .click({ force: true });
    cy.get(elements.btnloginEmailAndPassword).click({ force: true });
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });

    cy.get("body").then((body) => {
      cy.wait(6000);
      cy.url().then((url) => {
        if (url !== "https://www.usaflex.com.br/_secure/account/#/profile") {
          this.validNegative();
        } else if (
          url === "https://www.usaflex.com.br/_secure/account/#/profile"
        ) {
          if (pass === "invalidPass" || email === "invalidEmail") {
            throw new Error(
              "FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!"
            );
          }
          cy.get(elements.loginOk).should("have.text", "Olá");
          cy.get(elements.logo).click({ force: true });
        }
      });
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static acceptCookie() {
    cy.get(elements.cookies).click({ timeout: 30000 });
  }

  // static modalEmail() {
  //   cy.wait(2000);
   
    
   
  //   cy.get(elements.modalName, {timeout:4000}).type("teste teste");
  //   cy.get(elements.modalEmail).type("teste@teste.com.br");
  //   cy.get(elements.radioAccept).click({force:true});
  //   cy.get("button").contains("Girar a roleta").click({force:true});
  //   cy.get(elements.backSite, {timeout:3000}).click({force:true});
  // }

  static searchInvalidProduct(a:string,b:string){
    cy.get(`${a}`).type("123456");
    cy.get(`${b}`).dblclick({force:true})
    cy.wait(2000)
    cy.get('body').then(body=>{
      let nenhum = body.find(".rkt-search-not-found-message").length
      let ops = body.find(".j-empty__content").length
      if(nenhum > 0) {
        cy.get(".rkt-search-not-found-message").should('exist')
      } else if (ops > 0) {
        cy.get(".j-empty__content").should('exist')
      }
    })
  }


  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategories, { timeout: 3000 })
      .its("length")
      .then((length) => {
        cy.get(`${elements.linkCategories} > a`, { timeout: 3000 })
          .eq(Math.floor(Math.random() * length))
          .dblclick({ force: true });
      });
  }
}
