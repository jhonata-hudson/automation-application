import { Commons } from "./../commons";
import { gerarCpf } from "./utils";
declare var require: any;

export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  btnBuyCart: ".j-minicart-btn",
  calculeCep: "#shipping-calculate-link",
};

export class Checkout {
  static finish(logged: Boolean = true): void {
    Commons.inputCepAleatory(5,elements.inputCep, elements.calculeCep, true);
    cy.get(elements.btnFinish, { timeout: 3000 }).click({ force: true });
    if (!logged) {
      cy.get(elements.fieldEmail, { timeout: 3000 }).type(
        "teste@teste.com.br",
        { force: true }
      );
      cy.get(elements.btnSendMail).click({ force: true });
    }
    cy.wait(2000);
    Commons.inputDataName([
      elements.inputFirstName,
      elements.inputLastName,
      elements.inputCPF,
      elements.inputCellPhone,
    ]);
  }
  
}
