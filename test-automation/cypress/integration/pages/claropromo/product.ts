import { HomeClaroPromo } from "../claropromo/home";
export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-buy",
  fieldEmailBlack: "#email",
  btnNewsletter: "button",
  selectSize: ".prateleira > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  filterAllCategorys:
    ".j-category__searchNavigator > .navigation > div > div > h3 > a",
  filterSubCategorys: ".j-category__searchNavigator > .navigation > div > div",
  filterSubCategorysTxt:
    ".j-category__searchNavigator > .navigation > div > .search-single-navigator > h3",
  classDrinks: ".j-drinks__nav > div > ul > li ",
  nameProduct: ".fn.productName",
  drinksTypes: ".j-drinks__list",
  btnNextImg: ".slick-next.slick-arrow",
  ytb: ".receitas > .j-drinks__middle--video > iframe",
  btnFastBuy: ".j-shelf__item-actions-add-cart",
  btnBuyCart:
    ' div[class="j-box-button"] > a[class="buy-button buy-button-ref"]',
  stringProduct: ".j-product__name",
  btnBuyInCart: "#cart-to-orderform",
  btnKeepBuy: ".link-choose-more-products-wrapper",
  btnBuyMiniCart: ".j-minicart-items__list > li",
  btnBuyCartmin: ".j-minicart-btn",
};

export class Product {
  static filterProducts(
    random: boolean = true,
    category?: String,
    subCategory?: string
  ) {
    cy.get("body").then((body) => {
      if (random) {
        let amount = body.find(elements.filterAllCategorys).length;
        let categoryIndex = Math.floor(Math.random() * amount);
        cy.get(`${elements.filterAllCategorys}`)
          .eq(categoryIndex)
          .click({ force: true });
        cy.get(`${elements.filterSubCategorys}`).then(($res) => {
          let ul = $res.find("ul").length;
          let li = $res.find("ul").eq(categoryIndex).find("li").length;
          let categoryIndex2 = Math.floor(Math.random() * li);
          li === 1
            ? cy
                .get(`${elements.filterSubCategorys} > ul`)
                .eq(categoryIndex)
                .find("li > a")
                .eq(categoryIndex2)
                .click({ force: true })
            : cy
                .get(`${elements.filterSubCategorys} > ul`)
                .eq(Math.floor(Math.random() * ul))
                .find("li > a")
                .eq(Math.floor(Math.random() * li))
                .click({ force: true });
        });
      } else {
        cy.get(`${elements.filterAllCategorys}`)
          .contains(`${category!}`)
          .click({ force: true });
        cy.get(`${elements.filterSubCategorys} > ul > li > a`)
          .contains(`${subCategory!}`)
          .click({ force: true });
      }
    });
  }

  static selectProductRandomImage(): void {
    cy.get("body").then((body) => {
      let amount = body.find(elements.imageProduct).length;

      cy.log(String(amount));

      cy.wait(2000);

      if (amount === 0) {
        cy.go("back");
        HomeClaroPromo.accessCategoryAndSubCategory();
        this.selectProductRandomImage();
      } else {
        cy.get(elements.imageProduct)
          .its("length")
          .then((length) => {
            cy.get(elements.imageProduct)
              .eq(Math.floor(Math.random() * length))
              .click({ force: true });
          });
      }
    });
  }

  static goCartOrKeepBuying(retries?: any): void {
    cy.get(elements.btnBuyCart, { timeout: 3000 })
      .should("exist")
      .invoke("removeAttr", "target")
      .dblclick({ force: true });

    cy.wait(3000);

    cy.get("body").then((body) => {
      let miniCart = body.find(elements.btnBuyCartmin).length;

      let ceps = body.find(".checkout-container.row-fluid.cart-active").length;

      console.log(miniCart);

      console.log(ceps);

      if (miniCart === 0 && ceps === 0) {
        HomeClaroPromo.accessCategoryAndSubCategory();
        this.selectProductRandomImage();
        this.goCartOrKeepBuying(retries - 1);
      } else if (miniCart === 0 && retries! === 0) {
        throw new Error("Não foi possivel interagir com o Mini cart");
      } else if (miniCart === 1 && ceps === 0){
        cy.get(elements.btnBuyCartmin, { timeout: 3000 })
          .should("exist")
          .invoke("removeAttr", "target")
          .click({ force: true });
      }
    });
  }
}
