import { error } from "../../../../node_modules/cypress/types/jquery/index";
import { HomeFreixeNet } from "../freixenet/home";

export const elements: any = {
  banner: ".j-category__banner",
  subCategory: ".j-category__sub > li > a",
  imageProduct: ".j-shelf__item-img > .j-shelf__item-img__img > img",
  fieldEmailBlack: ".j-form",
  btnNewsletter: "button",
  selectSize: ".prateleira > a",
  linkCategoriesExact: "nav > ul > li > a",
  btnQuantity: ".fa.fa-plus",
  quantity: ".qtd",
  btnBuyProduct: ".buy-button",
  btnFinishBuy: "#cart-to-orderform",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  filterAllCategorys: ".search-multiple-navigator > fieldset",
  classDrinks: ".j-drinks__nav > div > ul > li ",
  nameProduct: ".fn.productName",
  drinksTypes: ".j-drinks__list",
  btnNextImg: ".slick-next.slick-arrow",
  ytb: ".receitas > .j-drinks__middle--video > iframe",
  btnFastBuy: ".j-shelf__item-actions-add-cart",
  btnBuyCart: 'a[class="j-minicart-btn"]',
  stringProduct: ".j-product__name",
  btnBuyInCart: "#cart-to-orderform",
  btnKeepBuy: ".link-choose-more-products-wrapper",
  success: ".j-success.is--active",
  logo: "#Ebene_1",
  cartIsActive: ".j-minicart__content.is--active",
  cartEmpty: ".j-minicart-empty",
  cartClose: ".j-minicart-close",
  unavaible: 'div[class="notifyme sku-notifyme"][style="display: none;"]',
};

export class Product {
  static selectDrink(): void {
    cy.get("a").contains("VER PRODUTO").click({ force: true });
    cy.get("body").then((body) => {
      let amount = body.find(elements.btnFastBuy).length;
      if (amount >= 1) {
        let categoryIndex = Math.floor(Math.random() * amount);
        cy.get(elements.btnFastBuy).eq(categoryIndex).click({ force: true });
      } else {
        this.warnMe();
        this.selectDrink();
      }
    });
  }

  static warnMe() {
    cy.go("back");
    cy.get(".slick-track").then((slicks) => {
      let slick = slicks.find('li[class="slick-slide"] > a').length;
      cy.get('li[class="slick-slide"] > a')
        .eq(Math.floor(Math.random() * slick))
        .click({ force: true });
    });
  }

  static selectRandomProduct(): void {
    cy.get("body").then((body) => {
      if (body.find(`${elements.drinksTypes} > div > div`).length > 0) {
        cy.get(`${elements.drinksTypes} > div > div`, { timeout: 4000 })
          .its("length")
          .then((doc) => {
            cy.get(`${elements.drinksTypes} > div > div > a`)
              .eq(Math.floor(Math.random() * doc))
              .click({ force: true });
          });
        this.selectDrink();
      } else if (body.find(elements.imageProduct).length > 0) {
        cy.get(elements.btnFastBuy, { timeout: 4000 })
          .its("length")
          .then((doc) => {
            cy.get(elements.btnFastBuy)
              .eq(Math.floor(Math.random() * doc))
              .click({ force: true });
          });
      }
    });
  }

  static goCartOrKeepBuying(): void {
    cy.wait(3000);

    
    cy.get(elements.cartIsActive).then((divs) => {
      let empty = divs.find(elements.cartEmpty).length;
      if (empty === 1) {
        this.validateCart();
      }
      cy.get(elements.btnBuyCart, { timeout: 3000 })
        .should("exist")
        .contains("Finalizar compra")
        .click({ force: true });
    });
  }

  static validateCart() {
    cy.get("body").then(() => {
      cy.get(elements.cartClose).click({ force: true });
      this.selectRandomProduct();
    });
  }
}
