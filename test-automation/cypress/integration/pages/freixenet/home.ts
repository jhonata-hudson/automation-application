import { eq } from "../../../../node_modules/cypress/types/lodash/index";

export const elements: any = {
  btnLoginEmail: "button > span",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnLogin: "#classicLoginBtn",
  loginOk: ".vtex-account__user-greeting > span",
  linkCategories: "nav > ul > li",
  linkCategoriesExact: "nav > ul > li > a",
  linkSubCategories: "a",
  subCategory: ".j-link__items--sub-menu > .j-brands__list > li > a",
  btnMyAcount: ".j-user-login > a",
  logo: "#Ebene_1",
  emailModal: "#email",
  nomeModal: "#nome",
  dateBirthModal: "#nascimento",
  phoneModal: "#telefone",
  btnConfirmModal: "#submit",
  acceptCookie: ".j-cookieAlert > button",
  loginEmailPass: "#loginWithUserAndPasswordBtn",
  filterAllCategorys: ".search-multiple-navigator > fieldset",
  classDrinks: ".j-drinks__nav > div > ul > li ",
  btnBuyProduct: ".buy-button.buy-button-ref",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  espumantes,
  vinhos,
  paraPresentear,
  kitEspecial,
  acessorios,
  outlet,
  drinks,
}
export class HomeFreixeNet {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnMyAcount).click({ force: true });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
          cy.get(elements.loginOk).should("have.text", "Olá");
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);

    cy.get(elements.btnMyAcount).click({ force: true });
    cy.get(elements.loginEmailPass).click({ force: true });
    cy.get(elements.loginEmailPass).click({ force: true });
    cy.get(elements.inputEmail).type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin).click({ force: true });
    cy.wait(4000);
    cy.get("body").then((url) => {
      let ola = url.find(elements.loginOk).length;
      if (ola === 1) {
        if (pass === "invalidPass" || email === "invalidEmail")
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!");
        cy.get(elements.loginOk).should("have.text", "Olá");
        cy.get(elements.logo).click({ force: true });
      } else if (ola === 0) {
        this.validNegative();
      }
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static homeLogo() {
    this.modalHome();

    cy.get(elements.logo).should("exist");
    this.acceptCookie();
  }

  static modalHome() {
    cy.get(elements.emailModal).type("teste@teste.com.br");
    cy.get(elements.nomeModal).type("teste teste");
    cy.get(elements.dateBirthModal).type("30041997");
    cy.get(elements.phoneModal).type("11911223344");
    cy.get(elements.btnConfirmModal).click();
    cy.wait(2000);
  }

  static acceptCookie() {
    cy.get(elements.acceptCookie).click({ timeout: 30000 });
  }

  static accessCategoryAndSubCategory(): void {
    cy.get(elements.linkCategoriesExact)
      .its("length")
      .then((length) => {
        let float = Math.random() * length;
        let idx = Math.floor(float);
        cy.get(elements.linkCategoriesExact).eq(idx).click();
      });
  }
}
