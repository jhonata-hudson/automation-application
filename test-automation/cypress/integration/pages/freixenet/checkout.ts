import { Commons } from "./../commons";
export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  fieldEmail: "#client-pre-email",
  btnSendMail: "#btn-client-pre-email",
  btnContinueShoop: "#btn-identified-user-button",
  imageProduct: ".j-shelf__item-img > .j-shelf__item-img__img > img",
  logo: "#Ebene_1",
  calculeCep: "#shipping-calculate-link",
};

export class Checkout {
  static finish(logged: Boolean = true): void {
    Commons.inputCepAleatory(5,elements.inputCep, elements.calculeCep,true);
    cy.get("a", { timeout: 3000 })
      .contains("Fechar pedido")
      .click({ force: true });
    if (logged) {
      cy.get(elements.fieldEmail).type("teste@teste.com.br");
      cy.get(elements.btnSendMail).click();
    } else {
      cy.reload();
      cy.get(elements.fieldEmail, { timeout: 3000 }).type("teste@teste.com.br");
      cy.get(elements.btnSendMail, { timeout: 3000 }).click();
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      )
    }
  }
}
