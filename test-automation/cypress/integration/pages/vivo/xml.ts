export enum NumberStr {
  "colecao" = 77,
  "considerados" = -44,
}

export class Xml {
  static percentage(
    newResult: Number,
    dataStore: String,
    dataUrl: String,
    typeXMl: string
  ) {
    cy.task("findCollectionData", { store: dataStore, xml: typeXMl }).then(
      (res: any) => {
  
        console.log("res", res);

        let lastResult =
          res.length !== 0 ? res[0]["indexedProducts"] : newResult;

        console.log("lastResult", lastResult);

        if (newResult >= lastResult) {
        
          cy.task("insertCollectionData", {
            indexedProducts: newResult,
            store: dataStore,
            xml: typeXMl,
            url: dataUrl,
          });
          cy.log(String(`Novo resultado adicionado: ${newResult} produtos`));

        } else {
          let collections = (Number(newResult) / lastResult) * 100;
          let finishResult = collections - 100;
        
          if (collections < 90) {
            throw new Error(
              `A diferença entre as operações é de ${Math.floor(
                Math.abs(finishResult)
              )}%. Ultimo dado ${lastResult}, Novo dado retornado: ${newResult}`
            );
          } else {
            cy.task("insertCollectionData", {
              indexedProducts: newResult,
              store: dataStore,
              xml: typeXMl,
              url: dataUrl,
            });
            cy.log(String(`Novo resultado adicionado: ${newResult} produtos`));
          }
        }
      }
    );
  }

  static validate(
    str: NumberStr,
    method: String,
    url: String,
    nameStr: String,
    store: String,
    typeXMl: string
  ) {
    cy.request({
      method: `GET`,
      url: `${url}`,
      timeout: 90000,
    }).then((response) => {
      console.log(response);

      new Promise(function () {
        expect(response.status).to.eq(200);
        
        let xml = response.body;
        let xmlSplit = String(xml).split("<!--");
        let index = xmlSplit[1].indexOf(`${nameStr}`);
        var result = xmlSplit[1].slice(index + `${nameStr}`.length + 2, str);
        
        cy.log(String(result))
      
        Xml.percentage(Number(result), store, url, typeXMl);
      
      });
    });
  }
}
