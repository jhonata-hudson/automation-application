import { Commons } from "./../commons";
declare var require: any;

export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  inputShipping: "#ship-postalCode",
  inputShipNumber: "#ship-number",
  inputShipDestiny: "#ship-receiverName",
  btnPayment: "#btn-go-to-payment",
  btnFinishCard: 'button : #payment-data-submit > [style="display: none;"]',
  calculeCep: "#shipping-calculate-link",
};

export class Checkout {
  static fill(): void {
  
    // Commons.inputCepAleatory(5,elements.inputShipping, elements.calculeCep, true);
    cy.get(elements.btnFinish).should("exist").click({ force: true });
    cy.wait(4000);

    cy.get(elements.inputFirstName, { timeout: 3000 }).should("exist").type("teste", {
      force: true,
    });
    cy.get(elements.inputLastName, { timeout: 3000 }).should("exist").type("teste", {
      force: true,
    });
    cy.get(elements.inputCPF, { timeout: 3000 }).should("exist").type("43214244884", {
      force: true,
    });
    cy.get(elements.inputCellPhone, { timeout: 3000 }).should("exist").type("1199999999", {
      force: true,
    });
    cy.get(elements.btnShipping).should("exist").dblclick({ force: true });
    cy.wait(3000);

    // cy.get("body").then((body) => {
    //   let ship = body.find(elements.inputShipNumber).length;

    //   if (ship === 0) {
    //     cy.reload();
    //   }

    //   cy.get(elements.inputShipNumber).should("exist").type("64", { force: true });
    //   cy.get(elements.inputShipDestiny).should("exist").type("teste", { force: true });
    // });
  }
  
  
  







}