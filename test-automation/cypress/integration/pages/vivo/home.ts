import { ElementFlags } from "../../../../node_modules/typescript/lib/typescript";

export const elements: any = {
  spanWelcome: "#js--close",
  linkCategories: ".menu-collection > ul > li > a",
  cookies: ".j-cookieAlert > button",
  modalEmail: "#wsew_email_input",
  btnSendEmail: "button",
  modalSale: ".banner_announcements--main",
  btnMyAcount: "a.account__link > .account__link",
  btnLoginEmail: "button > span",
  btnLogin: "#classicLoginBtn",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnSingIn: "#classicLoginBtn",
  loginOk: ".account__link.js-dataLayer-name-menu",
  removeProduct: ".cartProduct__remove.js-minicart-product-remove",
  cart: ".cart__count.js-minicart-count",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  modalLogin: "#vtexIdUI-auth-selector",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  todos,
  Smartphones,
  Informática,
  Casa,
  TV,
}

export class HomeVivo {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnMyAcount, { timeout: 3000 }).should("exist").click({ force: true });
    cy.get(elements.btnAcessEmailCode).should("exist").click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).should("exist").type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]').should("exist")
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).should("exist").type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).should("exist").click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).should("exist").type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).should("exist").type("00000000");
      cy.get(elements.enterWithCode).should("exist").click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static loginPage(pass: string, email: string) {
    cy.wait(3000);
    cy.get(elements.btnMyAcount, { timeout: 3000 }).should("exist").click({ force: true });
    cy.get(elements.btnloginEmailAndPassword).should("exist")
      .contains("Entrar com email e senha")
      .click({ force: true });
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "pJL[KU8E",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "automacao.qa@tatix.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.inputEmail).should("exist").type(infos[fieldEmail + 1], { force: true });
    cy.get(elements.inputPassword).should("exist").type(infos[fieldPass + 1], { force: true });
    cy.get(elements.btnLogin, { timeout: 3000 }).should("exist").click({ force: true });
    cy.wait(7000);
    cy.get("body").then((body) => {
      let modalLogin = body.find(elements.modalLogin).length;

      cy.log(String(modalLogin));

      if (modalLogin >= 1) {
        this.validNegative();
      } else if (modalLogin === 0) {
        if (pass === "invalidPass" || email === "invalidEmail") {
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS!!");
        }
      }
    });
    cy.wait(3000);
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static clearCart() {
    cy.get("body").then((body) => {
      if (body.find(elements.cart).text() === "1") {
        cy.get(elements.removeProduct).should("exist").invoke("show").click({ force: true });
      }
    });
  }

  static acceptCookie() {
    cy.get(elements.cookies).should("exist").click({ timeout: 30000 });
  }

  static modalSmartphone() {
    cy.wait(1000);
    cy.get("body").then((body) => {
      if (body.find(elements.modalSale).length > 0) {
        cy.get('a[class="banner_announcements--close"]').should("exist").click({ force: true });
      }
    });
  }

  static modalEmail() {
    cy.wait(1000);
    cy.get("body").then((body) => {
      if (body.find(elements.modalEmail).length > 0) {
        cy.wait(2000);
        cy.get(elements.modalEmail, {timeout:3000}).type(
          "teste@teste.com.br"
        );
        cy.get(elements.btnSendEmail, {timeout:3000}).should("exist")
          .contains("Enviar")
          .click({ force: true });
      }
    });
  }

  static accessCategory(random: boolean = true, category?: Categories): void {
    this.modalEmail();
    random
      ? cy
          .get(elements.linkCategories)
          .its("length")
          .then((length) =>
            cy
              .get(elements.linkCategories, { timeout: 4000 })
              .eq(Math.floor(Math.random() * (length - 2) + 1))
              .click({ force: true })
          )
      : cy.get(elements.linkCategories).eq(category!).click({ force: true });
    this.modalSmartphone();
  }
}
