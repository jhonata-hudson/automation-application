import { HomeVivo } from "../vivo/home";

export const elements: any = {
  productItem:
    ".productCard > .productCard__info > .productCard__price > .productCard__price--best",
  btnBuyProduct: "a.product-seller__button",
  productInfo: ".productCard__info",
  spanUnavaible: 'span[class="productCard__unavailable--visible"]',
  voltage: ".dropdown-select",
  productVolt: ".product-voltage",
  acceptVolt: ".modal__variation__product--confirm--mobile",
  imageProduct: ".productCard__image.js-shelf-product",
};

export class Product {
  static selectProduct(random: boolean = true, product?: string): void {
    HomeVivo.modalSmartphone();
    cy.get(elements.productItem, { timeout: 3000 })
      .its("length")
      .then((length: number) => {
        let idx = Math.floor(Math.random() * length);
        cy.get(elements.productInfo)
          .eq(idx)
          .then(($list) => {
            $list.find(elements.spanUnavaible).length > 0
              ? cy
                  .get(elements.imageProduct, { timeout: 3000 })
                  .eq(idx - 1)
                  .click({ force: true })
                  .click({ force: true })
              : cy
                  .get(elements.imageProduct, { timeout: 3000 })
                  .eq(idx)
                  .click({ force: true });
          });
      });
    this.warnMe();
    HomeVivo.modalEmail();
  }

  static warnMe(): void {
    cy.get("body").then((body) => {
      if (body.find(".product-seller__unavailable").length === 1) {
        cy.get('input[name="name-productUnavailable"]').type("teste teste", {
          force: true,
        });
        cy.get(
          'input[name="email-productUnavailable"]'
        ).type("teste@teste.com.br", { force: true });
        cy.get(
          'button[class="product-seller__unavailable--form__button"]'
        ).click({ force: true });
        cy.get(".product-seller__unavailable--alert").should("exist");
        cy.go('back')
        this.selectProduct()
      }
    });
  }

  static goCart(): void {
    HomeVivo.modalEmail();
    cy.get("body").then((body) => {
      if (body.find(elements.productVolt).length > 0) {
        cy.get(elements.voltage).click({ force: true });
        cy.get(elements.productVolt)
          .its("length")
          .then((volts) => {
            cy.get(elements.productVolt)
              .eq(Math.floor(Math.random() * volts))
              .click({ force: true });
          });
        if (body.find(elements.btnBuyProduct).length === 0) {
          cy.reload();
          this.goCart();
          cy.get(elements.btnBuyProduct).first().click({ force: true });
          cy.get(elements.acceptVolt)
            .contains("Sim, está correto")
            .click({ force: true });
        } else {
          cy.get(elements.btnBuyProduct).first().click({ force: true });
          cy.get(elements.acceptVolt)
            .contains("Sim, está correto")
            .click({ force: true });
        }
      } else {
        if (body.find(elements.btnBuyProduct).length === 0) {
          cy.reload();
          this.goCart();
          cy.get(elements.btnBuyProduct).first().click({ force: true });
        } else {
          cy.get(elements.btnBuyProduct).first().click({ force: true });
        }
      }
    });
  }
}
