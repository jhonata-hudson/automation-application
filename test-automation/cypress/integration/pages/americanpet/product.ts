import { HomeAmericanpet } from "../americanpet/home";

export const elements: any = {
  productItem: ".j-shelf__item-buy",
  btnWishList: ".btn.btn-block.btn-outline-secondary.y-wishBtn.btn-wishlist",
  selectModel: ".group_0",
  selectModelLabel: ".sku-selector-container > ul li span label",
  selectModelSpan: ".sku-selector-container > ul li span",
  btnBuyProduct: ".buy-button.buy-button-ref",
  divBuyInfo: ".buy-button.buy-button-ref",
  search: 'input[class="__rh-sb-input__ rh-sb-input"]',
  alertMe: 'div[class="notifyme-title-div"][style="display: block;"]',
  modalValidate: 'main > div[class="modal-validade"]',
};

export class ProductAmericanPet {
  static selectProduct(): void {
    cy.get(elements.productItem)
      .its("length")
      .then((length) => {
        cy.get(elements.productItem)
          .eq(Math.floor(Math.random() * length))
          .click({ force: true });
      });
  }

  static wishList(): void {
    cy.get(elements.btnWishList).click();
  }

  static selectModelAndBuy(): void {
    cy.get("body").then((body) => {
      if (body.find(elements.selectModelLabel).length > 0) {
        cy.get(elements.selectModelLabel)
          .its("length")
          .then((res) => {
            cy.get(elements.selectModelSpan).then((idx) => {
              if (idx.find('label[class$="disabled"]').length > 0) {
                for (let index = 0; index < res; index++) {
                  idx.find("label").eq(index).hasClass("disabled")
                    ? index++
                    : idx.find("label").eq(index).click();
                }
              } else {
                idx
                  .find("label")
                  .eq(Math.floor(Math.random() * res))
                  .click();
              }
            });
          });
      }
      cy.get(elements.btnBuyProduct).click({ force: true });
    });
  }

  static goCartOrKeepBuying(): void {
    cy.get("body").then((body) => {
      if (body.find(elements.alertMe).length === 1) {
        cy.go("back");
        this.selectProduct();
      }
      cy.get(".buy-button").first().click({ force: true });
      cy.get("body", { timeout: 2000 }).then((body) => {
        let modalValidate = body.find(elements.modalValidate).length;
        if (modalValidate === 1) {
          cy.get(elements.modalValidate).then(() => {
            cy.get('span[class="btn-sim"]', { timeout: 3000 }).click({
              force: true,
            });
          });
        }
      });
    });
  }
}
