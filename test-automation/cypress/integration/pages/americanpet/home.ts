import { assignIn } from "../../../../node_modules/cypress/types/lodash/index";

export const elements: any = {
  spanWelcome: "#js--close",
  linkCategories: 'div[class="j-header__menu"] > div > nav > ul > li',
  linkSubCategories: "a",
  btnLogin: ".j-list-login",
  btnloginEmailAndPassword: "#loginWithUserAndPasswordBtn",
  inputEmail: "#inputEmail",
  inputPassword: "#inputPassword",
  btnSingIn: "#classicLoginBtn",
  elCategoryUnavaible: "#busca-vazia-extra-middle",
  cookies: ".j-cookieAlert > button",
  logo: ".j-store__logo > a",
  marcas: ".j-header__drop--list.is--brands > .j-other > li",
  categorys: ".j-list__departments.is--desktop > li",
  subCategory: ".j-left > nav > ul > li",
  alert: ".alert.alert-warning.alert-wrong-pswd",
  btnAcessEmailCode: "#loginWithAccessKeyBtn",
  insertEmailToCode: ".controls.vtexIdUI-email-field > input",
  enterWithCode: "#confirmLoginAccessKeyBtn",
  accessCodeField: "#access-code",
};

export enum Categories {
  cachorro,
  gato,
  pássaro,
  peixe,
  roedores,
  farmácia,
  promo,
  marcas,
}

export const indxCategory: any = {
  0: "cachorro",
  1: "gato",
  2: "pássaro",
  3: "peixe",
  4: "roedores",
  5: "farmácia",
  6: "promo",
  7: "marcas",
};

export class HomeAmericanpet {
  static validCode(invalid?: String) {
    let inboxId;
    cy.get(elements.btnLogin).within(() => {
      cy.get("a").contains("Entrar").click({ force: true });
    });
    cy.get(elements.btnAcessEmailCode).click({ force: true });
    if (invalid) {
      cy.createInbox().then((inbox) => {
        assert.isDefined(inbox);
        inboxId = inbox.id;
        cy.get(elements.insertEmailToCode).type(`${inbox.emailAddress}`);
        cy.get('button[id="sendAccessKeyBtn"]')
          .contains("Confirmar")
          .click({ force: true });
        cy.waitForLatestEmail(inboxId).then((email) => {
          assert.isDefined(email);
          let content = email.subject;
          let result = content.match(/\d+/g).join(",");
          let index = result.split(",");
          let lastIndex = index.length;
          cy.get(elements.accessCodeField).type(`${index[lastIndex - 1]}`);
          cy.get(elements.enterWithCode).click({ force: true });
        });
      });
    } else {
      cy.get(elements.insertEmailToCode).type("teste@teste.com.br");
      cy.get('button[id="sendAccessKeyBtn"]')
        .contains("Confirmar")
        .click({ force: true });
      cy.get(elements.accessCodeField).type("00000000");
      cy.get(elements.enterWithCode).click({ force: true });
      cy.get("div")
        .contains("Chave de acesso inválida. Verifique a digitação.")
        .should("exist");
    }
  }

  static homeLogo() {
    cy.get(elements.logo).should("exist");
  }

  static loginPage(pass: string, email: string) {
    const infos = [
      "invalidPass",
      "SENHA-INVALIDA",
      "validPass",
      "Hc300497",
      "invalidEmail",
      "teste-invalido@gmail.com.br",
      "validEmail",
      "jhonata.oliveira@ezlogic.com.br",
    ];
    let fieldEmail = infos.indexOf(email);
    let fieldPass = infos.indexOf(pass);
    cy.get(elements.btnLogin).within(() => {
      cy.get("a").contains("Entrar").click({ force: true });
    });
    cy.get(elements.btnloginEmailAndPassword).click({ force: true });

    cy.get(elements.inputEmail, { timeout: 5000 }).type(infos[fieldEmail + 1], {
      force: true,
    });
    cy.get(elements.inputPassword, { timeout: 5000 }).type(
      infos[fieldPass + 1],
      { force: true }
    );
    cy.get(elements.inputPassword).type("{enter}");
    cy.wait(3000);
    cy.get("body").then((modal) => {
      let assign = modal.find(".vtex-account__menu-links").length;
      if (assign === 1) {
        if (pass === "invalidPass" || email === "invalidEmail")
          throw new Error("FOI POSSIVEL ACESSAR PAGINA COM DADOS INVALIDOS !!");
        cy.get(elements.logo).should("exist").click({ force: true });
      } else if (assign === 0) {
        this.validNegative();
      }
    });
  }

  static validNegative() {
    cy.get(`${elements.alert} > span`)
      .contains("Usuário e/ou senha errada")
      .should("exist");
  }

  static closeSpan() {
    cy.get("body").then((res) => {
      if (res.find(elements.spanWelcome).length > 0)
        res.find(elements.spanWelcome).click();
    });
  }
  static acceptCookie() {
    cy.get(elements.cookies)
      .contains("Entendi e Fechar")
      .click({ timeout: 30000 });
  }

  static accessCategoryAndSubCategory(): void {
    var textArray = [
      "cachorro",
      "gato",
      "passaro",
      "peixe",
      "roedores",
      "farmacia",
      "busca?fq=H:152",
    ];
    var randomNumber = Math.floor(Math.random() * textArray.length);
    var randomElement = textArray[randomNumber];
    cy.visit(`https://www.americanpet.com.br/${randomElement}`);
  }
}
