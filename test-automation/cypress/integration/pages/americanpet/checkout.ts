import { Commons } from "../../pages/commons";

export const elements: any = {
  btnFinish: "#cart-to-orderform",
  inputClientEmail: "#client-pre-email",
  btnClientEmail: "#btn-client-pre-email",
  inputFirstName: "#client-first-name",
  inputLastName: "#client-last-name",
  inputCPF: "#client-document",
  inputCellPhone: "#client-phone",
  btnShipping: "#go-to-shipping",
  btnContinueShopping: "#btn-identified-user-button",
  inputCep: "#ship-postalCode",
  inputNumberHouse: "#ship-number",
  btnFinishBuy: "#btn-go-to-payment",
  calculeCep: "#shipping-calculate-link",
  destiny: '#ship-receiverName'
};

export class Checkout {
  static finish(logged: boolean = true): void {
    Commons.inputCepAleatory(5,elements.inputCep, elements.calculeCep, true);
    cy.get(elements.btnFinish).click();
    if (logged) {
      Commons.inputDataName(
        [elements.inputFirstName,
        elements.inputLastName,
        elements.inputCPF,
        elements.inputCellPhone]
      );
    } else {
      cy.get(elements.inputClientEmail).type(`teste@teste.com.br`);
      cy.get(elements.btnClientEmail).click({ force: true });
      cy.wait(5000);
      cy.get(elements.btnContinueShopping, { timeout: 10000 }).click({
        force: true,
      });
      cy.get(elements.inputNumberHouse).type(
        `${Math.floor(Math.random() * 1)}`
      );
      cy.get(elements.destiny, { timeout: 2000 }).type('Teste',{force:true})
      // cy.get(elements.btnFinishBuy, { timeout: 2000 }).click({force:true});
    }
  }
}
