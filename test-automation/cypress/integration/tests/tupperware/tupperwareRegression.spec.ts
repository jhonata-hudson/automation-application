import { HomeTupperware } from "../../pages/tupperware/home";
import { Product } from "../../pages/tupperware/product";
import { Checkout } from "../../pages/tupperware/checkout";
import { Commons } from "../../pages/commons";

context("Tupperware Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({ capture: "runner" })
  });

 
  it("CT-01 Buy Products not Authenticate", () => {
    HomeTupperware.selectVisits("lojaVirtual");
    HomeTupperware.acceptCookie();
    HomeTupperware.accessCategoryAndSubCategory();
    Product.selectProductCategory();
    Product.goCart();
    Checkout.finish('false');
  });

  it("CT-02 Buy Products Authenticate", () => {
    HomeTupperware.selectVisits("login");
    HomeTupperware.loginSite("validPass", "validEmail");
    HomeTupperware.acceptCookie();
    HomeTupperware.accessCategoryAndSubCategory(false);
    Product.selectProductCategory();
    Product.goCart();
    Checkout.finish('true');
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeTupperware.selectVisits("login");
  //   HomeTupperware.loginSite("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeTupperware.selectVisits("login");
  //   HomeTupperware.loginSite("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeTupperware.selectVisits("login");
  // //   HomeTupperware.loginSite("validPass", "validEmail");
  // //   HomeTupperware.acceptCookie();
  // //   HomeTupperware.accessCategoryAndSubCategory(false);
  // //   Product.selectProductCategory();
  // //   Product.goCart();
  // //   Commons.validCep("buscaCep");
  // // });
  // it("CT-06 Delete product Cart", () => {
  //   HomeTupperware.selectVisits("login");
  //   HomeTupperware.loginSite("validPass", "validEmail");
  //   HomeTupperware.acceptCookie();
  //   HomeTupperware.accessCategoryAndSubCategory(false);
  //   Product.selectProductCategory();
  //   Product.goCart();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeTupperware.selectVisits("lojaVirtual");
  //   Commons.searchInvalid(
  //     "#Capa_1",
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar"
  //   );
  // });
});
