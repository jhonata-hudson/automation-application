import { HomeFreixeNet } from "../../pages/freixenet/home";
import { Product } from "../../pages/freixenet/product";
import { Checkout } from "../../pages/freixenet/checkout";

context("Freixenet Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.freixenet.com.br");
  });

  afterEach(() => {
    cy.wait(3000);
    cy.screenshot({ capture: "runner" });
  });


  it("CT-01 Buy random product not logged", () => {
    HomeFreixeNet.homeLogo();
    HomeFreixeNet.accessCategoryAndSubCategory();
    Product.selectRandomProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });

  it("CT-02 Add random product in cart", () => {
    HomeFreixeNet.homeLogo();
    HomeFreixeNet.accessCategoryAndSubCategory();
    Product.selectRandomProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeFreixeNet.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeFreixeNet.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeFreixeNet.homeLogo();
  // //   HomeFreixeNet.accessCategoryAndSubCategory();
  // //   Product.selectRandomProduct();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep", "#client-pre-email");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeFreixeNet.homeLogo();
  //   HomeFreixeNet.accessCategoryAndSubCategory();
  //   Product.selectRandomProduct();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });


  // it("CT-07 search product invalid", () => {
  //   HomeFreixeNet.homeLogo();
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-not-found__text"
  //   );
  // });
});
