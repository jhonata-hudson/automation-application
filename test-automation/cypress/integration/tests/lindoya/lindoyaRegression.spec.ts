import { HomeLindoya } from "../../pages/lindoya/home";
import { Product } from "../../pages/lindoya/product";
import { Checkout } from "../../pages/lindoya/checkout";
import { Commons } from "../../pages/commons";

context("Lindoya Verão Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.lindoyaverao.com.br/");
    HomeLindoya.acceptCookie();
  });

  afterEach(() => {
    cy.wait(3000);
    cy.screenshot({ capture: "runner" })
  });

  
  it("CT-01 Buy Product not logged", () => {
    HomeLindoya.accessCategoryAndSubCategory();
    Product.selectProductRandom();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });


  it("CT-02 Buy Product logged", () => {
    HomeLindoya.loginPage("validPass", "validEmail");
    HomeLindoya.accessCategoryAndSubCategory();
    Product.selectProductRandom();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeLindoya.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeLindoya.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeLindoya.loginPage("validPass", "validEmail");
  // //   HomeLindoya.accessCategoryAndSubCategory();
  // //   Product.selectProductRandom();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeLindoya.loginPage("validPass", "validEmail");
  //   HomeLindoya.accessCategoryAndSubCategory();
  //   Product.selectProductRandom();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeLindoya.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });
});
