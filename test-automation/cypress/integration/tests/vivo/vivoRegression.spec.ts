import { HomeVivo } from "../../pages/vivo/home";
import { Product } from "../../pages/vivo/product";
import { Checkout } from "../../pages/vivo/checkout";
import { Commons } from "../../pages/commons";

context("Vivo Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://loja.vivo.com.br/");
    HomeVivo.acceptCookie();
  });

  afterEach(() => {
    cy.screenshot({ capture: "runner" });
  });

  it("CT-01 Checkout logged", () => {
    HomeVivo.loginPage("validPass", "validEmail");
    HomeVivo.clearCart();
    HomeVivo.accessCategory();
    Product.selectProduct();
    Product.goCart();
    Checkout.fill();
    if (Cypress.isBrowser("firefox")) {
      cy.getCookies({ log: false }).then((cookies) =>
        cookies.forEach((cookie) => cy.clearCookie(cookie.name, { log: false }))
      );
      cy.log("clearCookies");
    } else {
      cy.clearCookies();
    }
  });

  it("CT-02 payment logged", () => {
    HomeVivo.loginPage("validPass", "validEmail");
    HomeVivo.clearCart();
    HomeVivo.accessCategory();
    Product.selectProduct();
    Product.goCart();
    Checkout.fill();
    if (Cypress.isBrowser("firefox")) {
      cy.getCookies({ log: false }).then((cookies) =>
        cookies.forEach((cookie) => cy.clearCookie(cookie.name, { log: false }))
      );
      cy.log("clearCookies");
    } else {
      cy.clearCookies();
    }
  });
  //    it("CT-03 Login page with email valid and password invalid", () => {
  //      HomeVivo.loginPage("invalidPass", "validEmail");
  //    });

  //    it("CT-04 Login page with email invalid and password valid", () => {
  //      HomeVivo.loginPage("validPass", "invalidEmail");
  //    });

  //     // it("CT-05 Checkout with i do not know my CEP", () => {
  //     //   HomeVivo.loginPage("validPass", "validEmail");
  //     //   HomeVivo.clearCart();
  //     //   HomeVivo.accessCategory();
  //     //   Product.selectProduct();
  //     //   Product.goCart();
  //     //   Commons.validCep("buscaCep");
  //     // });

  //    it("CT-06 Delete product Cart", () => {
  //      HomeVivo.loginPage("validPass", "validEmail");
  //      HomeVivo.clearCart();
  //      HomeVivo.accessCategory();
  //      Product.selectProduct();
  //      Product.goCart();
  //      Commons.deleteCart(
  //        'td[class="item-remove"] > a[class="item-link-remove"]',
  //        'div[class="empty-cart-content"][style="display: block;"]'
  //      );
  //    });

  //    it("CT-07 search product invalid", () => {
  //      HomeVivo.loginPage("validPass", "validEmail");
  //      Commons.searchInvalid(
  //        ".search__input",
  //        "#formSearchInput",
  //        ".rkt-search-icon.search"
  //      );
  //    });
});
