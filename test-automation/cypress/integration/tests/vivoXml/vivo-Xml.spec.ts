import { NumberStr, Xml } from "../../pages/vivo/xml";

context("Vivo Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
  });


  afterEach(() => {
    cy.screenshot({ capture: "runner" });
  });


  it("CT-01 Validate XML Facebook", () => {
   
  
    Xml.validate(NumberStr.colecao,'GET', 'http://www.loja.vivo.com.br/XMLData/facebook.xml','coleção', 'vivo', 'facebook')
  });


  it("CT-02 Validates XML Facebook", () => {
   
  
    Xml.validate(NumberStr.colecao,'GET', 'http://www.loja.vivo.com.br/XMLData/facebook.xml','coleção', 'vivo', 'facebook')
  });
});
