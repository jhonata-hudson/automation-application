import { Commons } from "../../pages/commons";
import { Home } from "../../pages/ambev/home";
import { Product } from "../../pages/ambev/product";
import { Checkout } from "../../pages/ambev/checkout";
declare var require: any;

context("Franquia Ambev Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.franquiaambev.com.br/");
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({ capture: "runner" })
  });

  
  it("CT-01 Buy product Authenticate valid", () => {
    Product.goCartOrKeepBuying();
    Home.loginPage();
    Home.accessCategoryAndSubCategory();
    Product.selectProduct();
    // Checkout.finish();
  });



  it("CT-02 Buy other product not Authenticate invalid", () => {
    Home.loginPage();
    Home.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    // Checkout.finish();
  });
});