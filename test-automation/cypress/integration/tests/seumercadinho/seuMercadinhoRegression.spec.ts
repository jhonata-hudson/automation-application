import { HomeMercadinho } from "../../pages/seumercadinho/home";
import { Product } from "../../pages/seumercadinho/product";
import { Checkout } from "../../pages/seumercadinho/checkout";
import { Commons } from "../../pages/commons";

context("Seu Mercadinho Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.seumercadinho.com.br/Sistema/401");
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({ capture: "runner" })
  });


 

  it("CT-01 Buy product Authenticate", () => {
    HomeMercadinho.loginPage("validPass", "validEmail");
    HomeMercadinho.acceptCookie();
    HomeMercadinho.clearCart();
    HomeMercadinho.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  
  it("CT-02 Buy product Authenticate", () => {
    HomeMercadinho.loginPage("validPass", "validEmail");
    HomeMercadinho.acceptCookie();
    HomeMercadinho.clearCart();
    HomeMercadinho.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeMercadinho.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeMercadinho.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeMercadinho.loginPage("validPass", "validEmail");
  // //   HomeMercadinho.acceptCookie();
  // //   HomeMercadinho.clearCart();
  // //   HomeMercadinho.accessCategoryAndSubCategory();
  // //   Product.selectProduct();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeMercadinho.loginPage("validPass", "validEmail");
  //   HomeMercadinho.acceptCookie();
  //   HomeMercadinho.clearCart();
  //   HomeMercadinho.accessCategoryAndSubCategory();
  //   Product.selectProduct();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });


  // it("CT-07 search product invalid", () => {
  //   HomeMercadinho.loginPage("validPass", "validEmail");
  //   HomeMercadinho.acceptCookie();
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });


  
});
