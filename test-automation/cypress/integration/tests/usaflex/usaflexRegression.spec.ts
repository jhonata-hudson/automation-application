import { HomeUsaflex } from "../../pages/usaflex/home";
import { Product } from "../../pages/usaflex/product";
import { Checkout } from "../../pages/usaflex/checkout";
import { Commons } from "../../pages/commons";

context("Usaflex Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.usaflex.com.br/");
    HomeUsaflex.acceptCookie();
  });

  afterEach(() => {
    cy.wait(6000);
    cy.screenshot({capture:'runner'});
  });


  it("CT-01 Checkout not Authenticate", () => {
    // HomeUsaflex.modalEmail();
    HomeUsaflex.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.selectModelAndBuy();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });

  
  it("CT-02 Buy product Authenticate", () => {
    // HomeUsaflex.modalEmail();
    HomeUsaflex.login("validPass", "validEmail");
    HomeUsaflex.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.selectModelAndBuy();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeUsaflex.modalEmail();
  //   HomeUsaflex.login("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeUsaflex.modalEmail();
  //   HomeUsaflex.login("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeUsaflex.modalEmail();
  // //   HomeUsaflex.login("validPass", "validEmail");
  // //   HomeUsaflex.accessCategoryAndSubCategory();
  // //   Product.selectProduct();
  // //   Product.selectModelAndBuy();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });
  // it("CT-06 Delete product Cart", () => {
  //   HomeUsaflex.modalEmail();
  //   HomeUsaflex.login("validPass", "validEmail");
  //   HomeUsaflex.accessCategoryAndSubCategory();
  //   Product.selectProduct();
  //   Product.selectModelAndBuy();
  //   Product.goCartOrKeepBuying();
  //   cy.get('td[class="item-remove"] > a[class="item-link-remove"]').click({
  //     force: true,
  //   });
  //   cy.wait(3000);
  //   cy.get('td[class="item-remove"] > a[class="item-link-remove"]').click({
  //     force: true,
  //   });
  //   cy.get('div[class="empty-cart-content"][style="display: block;"]').should(
  //     "exist"
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeUsaflex.modalEmail();
  //   HomeUsaflex.login("validPass", "validEmail");
  //   HomeUsaflex.searchInvalidProduct(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar"
  //   );
  // });
});
