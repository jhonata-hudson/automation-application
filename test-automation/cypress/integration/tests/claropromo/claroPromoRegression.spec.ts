import { HomeClaroPromo } from "../../pages/claropromo/home";
import { Commons } from "../../pages/commons";
import { Product } from "../../pages/claropromo/product";
import { Checkout } from "../../pages/claropromo/checkout";

context("Claro Promo Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.claropromo.com.br/");
    HomeClaroPromo.acceptCookie();
  });

  afterEach(() => {
    cy.wait(3000);
    cy.screenshot();
  });
  

  it("CT-01 Buy Product not logged", () => {
    HomeClaroPromo.accessCategoryAndSubCategory();
    Product.selectProductRandomImage();
    Product.goCartOrKeepBuying(3);
    Checkout.finish(false);
  });


  it("CT-02 Buy Product logged", () => {
    HomeClaroPromo.loginPage("validPass", "validEmail");
    cy.wait(3000)
    HomeClaroPromo.accessCategoryAndSubCategory();
    Product.selectProductRandomImage();
    Product.goCartOrKeepBuying(3);
    Checkout.finish();
  });

  
  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeClaroPromo.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeClaroPromo.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeClaroPromo.loginPage("validPass", "validEmail");
  // //   HomeClaroPromo.accessCategoryAndSubCategory();
  // //   Product.selectProductRandomImage();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeClaroPromo.loginPage("validPass", "validEmail");
  //   HomeClaroPromo.accessCategoryAndSubCategory();
  //   Product.selectProductRandomImage();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeClaroPromo.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });
});
