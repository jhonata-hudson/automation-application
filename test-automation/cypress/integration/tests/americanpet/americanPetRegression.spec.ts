import { Commons } from "../../pages/commons";
import { Categories, HomeAmericanpet } from "../../pages/americanpet/home";
import { elements, ProductAmericanPet } from "../../pages/americanpet/product";
import { Checkout } from "../../pages/americanpet/checkout";

context("American Pets Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.americanpet.com.br/");
    HomeAmericanpet.acceptCookie();
  });

  afterEach(() => {
    // cy.wait(6000);

    cy.screenshot({ capture: "runner" })
  });


  it("CT-01 Buy product not Authenticate", () => {
    HomeAmericanpet.accessCategoryAndSubCategory();
    ProductAmericanPet.selectProduct();
    ProductAmericanPet.goCartOrKeepBuying();
    Checkout.finish(false);
  });
  
  it("CT-02 Buy product Authenticate", () => {
    HomeAmericanpet.loginPage("validPass", "validEmail");
    HomeAmericanpet.accessCategoryAndSubCategory();
    ProductAmericanPet.selectProduct();
    ProductAmericanPet.goCartOrKeepBuying();
    Checkout.finish(true);
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeAmericanpet.loginPage("invalidPass", "validEmail");
  // });
  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeAmericanpet.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeAmericanpet.loginPage("validPass", "validEmail");
  // //   HomeAmericanpet.accessCategoryAndSubCategory();
  // //   ProductAmericanPet.selectProduct();
  // //   ProductAmericanPet.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeAmericanpet.loginPage("validPass", "validEmail");
  //   HomeAmericanpet.accessCategoryAndSubCategory();
  //   ProductAmericanPet.selectProduct();
  //   ProductAmericanPet.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // // it("CT-06 Checkout with invalid CEP", () => {
  // //   HomeAmericanpet.loginPage("validPass", "validEmail");
  // //   HomeAmericanpet.accessCategoryAndSubCategory();
  // //   ProductAmericanPet.selectProduct();
  // //   ProductAmericanPet.goCartOrKeepBuying();
  // //   Checkout.finish(true, "invalidCep");
  // // });

  // it("CT-07 search product invalid", () => {
  //   HomeAmericanpet.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });
});
