import { HomeEverest } from "../../pages/softEverest/home";
import { Product } from "../../pages/softEverest/product";
import { Checkout } from "../../pages/softEverest/checkout";
import { Commons } from "../../pages/commons";

context("Softys Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.softpurificadores.com.br/");
    HomeEverest.acceptCookie();
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot();
  });
  
  it("CT-01 Buy product not Authenticate", () => {
    HomeEverest.clearCart()
    HomeEverest.accessCategoryAndSubCategory();
    // Product.selectProductRandom();
    // Product.goCartOrKeepBuying(5);
    // Checkout.finish(false);
  });


  it("CT-02 Buy product Authenticate", () => {
    HomeEverest.loginPage("validPass", "validEmail");
    HomeEverest.clearCart()
    HomeEverest.accessCategoryAndSubCategory();
    // Product.selectProductRandom();
    // Product.goCartOrKeepBuying(5);
    // Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeEverest.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeEverest.loginPage("validPass", "invalidEmail");
  // });

  // //it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeEverest.loginPage("validPass", "validEmail");
  // //   HomeEverest.accessCategoryAndSubCategory();
  // //   Product.selectProductRandom();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeEverest.loginPage("validPass", "validEmail");
  //   HomeEverest.accessCategoryAndSubCategory();
  //   Product.selectProductRandom();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeEverest.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty-shelf"
  //   );
  // });
});
