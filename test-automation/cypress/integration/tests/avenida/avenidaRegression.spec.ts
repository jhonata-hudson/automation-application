import { Commons } from "../../pages/commons";
import { Home } from "../../pages/avenida/home";
import { Product } from "../../pages/avenida/product";
import { Checkout } from "../../pages/avenida/checkout";

context("Avenida Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.avenida.com.br/");

    Home.closeSpan();
    Home.acceptCookie();
  });

  
  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({capture:'runner'});
  });


  it.only("CT-01 Checkout not Authenticate", () => {    
    Home.accessCategoryAndSubCategory();
    Product.selectProduct(6);
    Product.selectModelAndBuy();
    Product.goCartOrKeepBuying();
     Checkout.finish('true')
    if (Cypress.isBrowser("firefox")) {
      cy.getCookies({ log: false }).then((cookies) =>
        cookies.forEach((cookie) => cy.clearCookie(cookie.name, { log: false }))
      );
      cy.log("clearCookies");
    } else {
      cy.clearCookies();
    }
  });


  it("CT-02 Checkout Authenticate", () => {
    // Home.login("validPass", "validEmail");
    // Home.accessCategoryAndSubCategory();
    // Product.selectProduct(6);
    // Product.selectModelAndBuy();
    // Product.goCartOrKeepBuying();
    //  Checkout.finish('false');
    //   if (Cypress.isBrowser("firefox")) {
    //     cy.getCookies({ log: false }).then((cookies) =>
    //       cookies.forEach((cookie) => cy.clearCookie(cookie.name, { log: false }))
    //     );
    //     cy.log("clearCookies");
    //   } else {
    //     cy.clearCookies();
    //   }
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   Home.login("invalidPass", "validEmail");
  // });
  // it("CT-04 Login page with email invalid and password valid", () => {
  //   Home.login("invalidPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   Home.login("validPass", "validEmail");
  // //   Home.accessCategoryAndSubCategory();
  // //   Product.selectProduct();
  // //   Product.selectModelAndBuy();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   Home.login("validPass", "validEmail");
  //   Home.accessCategoryAndSubCategory();
  //   Product.selectProduct();
  //   Product.selectModelAndBuy();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   Home.login("validPass", "validEmail");
  //   cy.visit(
  //     "https://www.avenida.com.br/pesquisa/?q=123456789&pageType=notfound"
  //   );
  //   cy.get(".j-shelf__item-img").should("not.exist");
  // });

  //it("CT-06 Checkout with invalid CEP", () => {
  // Home.login("validPass", "validEmail");
  // Home.accessCategoryAndSubCategory();
  // Product.selectProduct();
  // Product.selectModelAndBuy();
  // Product.goCartOrKeepBuying();
  // Checkout.finish(true, "invalidCep");
  //});
});
