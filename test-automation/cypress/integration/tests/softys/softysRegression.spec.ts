import { HomeSoftys } from "../../pages/softys/home";
import { Product } from "../../pages/softys/product";
import { Checkout } from "../../pages/softys/checkout";
import { Commons } from "../../pages/commons";

context("Softys Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.lojasoftys.com.br/");
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({capture:'runner'});
  });
  
  
  
  it("CT-01 Buy product not Authenticate", () => {
    HomeSoftys.clearCart();
    HomeSoftys.accessCategoryAndSubCategory();
    Product.selectProductCategory();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });

  it("CT-02 Buy product Authenticate", () => {
    HomeSoftys.loginPage("validPass", "validEmail");
    HomeSoftys.clearCart();
    HomeSoftys.accessCategoryAndSubCategory();
    Product.selectProductCategory();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeSoftys.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeSoftys.loginPage("validPass", "invalidEmail");
  // });

  // //it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeSoftys.loginPage("validPass", "validEmail");
  // //   HomeSoftys.clearCart();
  // //   HomeSoftys.accessCategoryAndSubCategory();
  // //   Product.selectProductCategory();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeSoftys.loginPage("validPass", "validEmail");
  //   HomeSoftys.clearCart();
  //   HomeSoftys.accessCategoryAndSubCategory();
  //   Product.selectProductCategory();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeSoftys.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar"
  //   );
  // });
});
