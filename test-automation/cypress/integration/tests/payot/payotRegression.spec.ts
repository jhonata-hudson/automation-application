import { HomePayot } from "../../pages/payot/home";
import { Product } from "../../pages/payot/product";
import { CheckoutPayot } from "../../pages/payot/checkout";
import { Commons } from "../../pages/commons";

context("Payot Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.lojapayot.com.br/");
    HomePayot.acceptCookie();
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({ capture: "runner" })
  });
 
  
  it("CT-01 Buy product not Authenticate", () => {
    
    HomePayot.closeSpan();
    HomePayot.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    CheckoutPayot.finish(false);
    // HomePayot.closeSpan();
  });

  it("CT-02 Buy product Authenticate", () => {
    HomePayot.closeSpan();
    HomePayot.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    //HomePayot.closeSpan();
    CheckoutPayot.finish(false);
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomePayot.closeSpan();
  //   HomePayot.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomePayot.closeSpan();
  //   HomePayot.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomePayot.closeSpan();
  // //   HomePayot.accessCategoryAndSubCategory();
  // //   Product.selectProduct();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep", "#client-pre-email");
  // // });
  
  // it("CT-06 Delete product Cart", () => {
  //   HomePayot.closeSpan();
  //   HomePayot.accessCategoryAndSubCategory();
  //   Product.selectProduct();
  //   Commons.deleteCart(".j-minicart-remove__item", ".j-minicart-empty");
  // });

  // it("CT-07 search product invalid", () => {
  //   HomePayot.closeSpan();
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });
});
