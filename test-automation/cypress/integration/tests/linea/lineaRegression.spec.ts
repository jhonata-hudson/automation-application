import { HomeLinea } from "../../pages/linea/home";
import { Product } from "../../pages/linea/product";
import { Checkout } from "../../pages/linea/checkout";
import { Commons } from "../../pages/commons";

context("Linea Regression Tests", () => {
  beforeEach(() => {
    cy.on("uncaught:exception", () => false);
    cy.visit("https://www.lineaalimentos.com.br/");
  });

  afterEach(() => {
    cy.wait(1000);
    cy.screenshot({ capture: "runner" });
  });
  
  it("CT-01 Buy product not Authenticate", () => {
    HomeLinea.modalHome();
    HomeLinea.acceptCookie();
    HomeLinea.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish(false);
  });

  it("CT-02 Buy product Authenticate", () => {
    HomeLinea.modalHome();
    HomeLinea.acceptCookie();
    HomeLinea.loginPage("validPass", "validEmail");
    HomeLinea.accessCategoryAndSubCategory();
    Product.selectProduct();
    Product.goCartOrKeepBuying();
    Checkout.finish();
  });

  // it("CT-03 Login page with email valid and password invalid", () => {
  //   HomeLinea.modalHome();
  //   HomeLinea.acceptCookie();
  //   HomeLinea.loginPage("invalidPass", "validEmail");
  // });

  // it("CT-04 Login page with email invalid and password valid", () => {
  //   HomeLinea.modalHome();
  //   HomeLinea.acceptCookie();
  //   HomeLinea.loginPage("validPass", "invalidEmail");
  // });

  // // it("CT-05 Checkout with i do not know my CEP", () => {
  // //   HomeLinea.modalHome();
  // //   HomeLinea.acceptCookie();
  // //   HomeLinea.accessCategoryAndSubCategory();
  // //   Product.selectProduct();
  // //   Product.goCartOrKeepBuying();
  // //   Commons.validCep("buscaCep");
  // // });

  // it("CT-06 Delete product Cart", () => {
  //   HomeLinea.modalHome();
  //   HomeLinea.acceptCookie();
  //   HomeLinea.accessCategoryAndSubCategory();
  //   Product.selectProduct();
  //   Product.goCartOrKeepBuying();
  //   Commons.deleteCart(
  //     ".item-link-remove",
  //     'div[class="empty-cart-content"][style="display: block;"]'
  //   );
  // });

  // it("CT-07 search product invalid", () => {
  //   HomeLinea.modalHome();
  //   HomeLinea.acceptCookie();
  //   HomeLinea.loginPage("validPass", "validEmail");
  //   Commons.searchInvalid(
  //     ".fulltext-search-box.ui-autocomplete-input",
  //     ".btn-buscar",
  //     ".j-empty__content"
  //   );
  // });
});
