/// <reference path="../../support/index.d.ts" />

import baseElements from "./baseElements";
class basePage {
    login() {
        cy.logindevbackoffice();
        cy.visit('/change-log');
        cy.get(baseElements.cbo_lojas).click()
            .type('8999 {enter}')
            .as('combobox');
        cy.get('@combobox').contains('8999 - artex Escritório Paulista 8999');
    }

    validarAlertaMensagem() {
        return cy.get(baseElements.alert_message, { timeout: 10000 });
    }

    fecharAlertaMensagem() {
        cy.get(baseElements.alert_message).then(($alert) => {
            if ($alert.show) {
                cy.get(baseElements.alert_message)
                    .parent()
                    .contains('Atualize o quadro de funcionários')
                    .get(baseElements.icon_close_alert_message).eq(1)
                    .click();
                cy.log('Foi possível fechar o alerta do Quadro de Funcionários');
            } else {
                cy.log('Não foi possível fechar o alerta do Quadro de Funcionários');
            }
        })
    }

    deletarArquivosDownloads() {
        cy.exec('rm cypress/downloads/*', { log: true, failOnNonZeroExit: false });
    }

    getFilenameAndValidateXlsx(data) {
        cy.wait(5000);
        const path = 'cypress/downloads/';
        cy.readFiles(path).then(result => {
            cy.parseXlsx(`${path}${result[0]}`).then(
                jsonData => {
                    expect(jsonData[0].data[0]).to.eqls(data);
                }
            );
        })
    }
}

export default new basePage();