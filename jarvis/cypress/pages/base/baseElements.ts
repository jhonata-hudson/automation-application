export const baseElements = {
    // Alertas
    alert_message: 'div[class$="notification-notice-closable"]',
    icon_close_alert_message: 'a[class="ant-notification-notice-close"]',
    cbo_lojas: 'div[role="combobox"]'
}

export default baseElements;