export const inventarioElements = {
    // Menu Inventário
    nav_inventario: 'i[class$="ordered-list"]',
    li_listar_inventario: 'a[href="/inventory"]',

    // Tela Inicial Inventário
    btn_novo_inventario: 'button[class$="ant-btn-primary"]',
    tab_inventario: 'div[class="ant-table-wrapper"]',
    col_title: 'span[class="ant-table-column-title"]',
    tr_inventario: 'tr[class$="ant-table-row-level-0"]',
    lbl_status: 'span[class="ant-badge-status-text"]',
    icon_download: 'svg[data-icon="download"]',
    icon_editar: 'svg[data-icon="edit"]',

    // Importação de Dados
    icon_voltar: 'i[class$="arrow-left"]',
    txt_loja: 'input[placeholder="Selecionar Loja"]',
    btn_upload: 'input[type="file"]',
    icon_remover_arquivo: 'a[title="Remover arquivo"]',
    icon_switch_sim_nao: 'span[class$="ant-switch-inner"]',
    btn_continuar: 'button[class$="ant-btn-primary"]',

    // Tela Análise e Confronto
    txt_segunda_contagem: 'input[type="text"]',
    icon_imprimir: 'button[class$="ant-btn-link"]',
    icon_numero_secao: 'span[class$="ant-tag ant-tag-blue"]',
    btn_finalizar: 'button[class$="ant-btn-primary"]',
    tab_combinados: 'div[class$=ant-tabs-tab]',
    lbl_nao_existe_dados: 'div p[class$="empty-description"]',
    lbl_secao: 'div span[class$="has-color"]',
    icon_check: 'i[class*=anticon-check]',
    modal_secao: 'div[class$="ant-modal-content"]',
    modal_finalizar_inventario: 'div[class="ant-modal-body"]',

    // Tela Finalização
    check_circle: 'i[class*="check-circle"]',
    lbl_mensagem_sucesso: 'div[class="ant-typography"]',
}

export default inventarioElements;