/// <reference path="../../support/index.d.ts" />

import basePage from '../base/basePage';
import inventarioElements from './inventario.elements';

class inventarioPage {
    //Eventos de Click()
    clicarMenuInventario() {
        cy.get(inventarioElements.nav_inventario).click();
        cy.get(inventarioElements.li_listar_inventario).click();
    }

    clicarBtnNovoInvetario() {
        cy.get(inventarioElements.btn_novo_inventario).click();
    }

    clicarBtnContinuar() {
        cy.get(inventarioElements.btn_continuar).click();
    }

    clicarBtnFinalizar() {
        cy.get(inventarioElements.btn_finalizar).click();
    }

    clicarFinalizarInventario() {
        cy.get(inventarioElements.modal_finalizar_inventario).click();
        cy.contains('Sim').click();
    }

    clicarIconeVoltar() {
        cy.get(inventarioElements.icon_voltar).click();
    }

    clicarBtnEditar() {
        cy.get(inventarioElements.tr_inventario).eq(0).get(inventarioElements.icon_editar).eq(0).click();
    }

    clicarIconeDownload() {
        cy.get(inventarioElements.tr_inventario).eq(0).get(inventarioElements.icon_download).eq(0).click();
    }

    clicarRemoverArquivo() {
        cy.get(inventarioElements.icon_remover_arquivo).invoke('show').click();
    }

    clicarIconeImprimirResultados() {
        cy.get(inventarioElements.icon_imprimir).click();
    }

    clicarImprimirResultadosSecao() {
        cy.get(inventarioElements.icon_numero_secao).eq(0).click();
        cy.get(inventarioElements.modal_secao).click().contains('Imprimir seção').click();
    }

    clicarAbaCombinados() {
        cy.get(inventarioElements.tab_combinados).contains('Combinados').click();
    }

    clicarSwitchNao() {
        cy.get(inventarioElements.icon_switch_sim_nao).click();
    }

    clicarAbaItensNaoContabilizados() {
        cy.get(inventarioElements.tab_combinados).contains('Itens não contablizados').click();
    }

    // Eventos de Seleção
    selecionarLoja(loja) {
        cy.get(inventarioElements.txt_loja).type(loja + '{enter}')
    }

    selecionarArquivo(arquivo) {
        cy.get(inventarioElements.btn_upload).attachFile(arquivo, { allowEmpty: true });
    }

    // Eventos de Preenchimento de Infos
    preencherCamposSegundaContagem() {
        cy.get(inventarioElements.tr_inventario).parent().find(inventarioElements.txt_segunda_contagem).eq(0).type('0');
        cy.get(inventarioElements.tr_inventario).parent().find(inventarioElements.txt_segunda_contagem).eq(1).type('0');
    }

    // Eventos de Validação
    validarSucessoInvetario() {
        cy.get(inventarioElements.check_circle).should('be.visible');
        cy.get(inventarioElements.lbl_mensagem_sucesso).contains('Finalizado com sucesso!');
    }

    validarStatusEmProgresso() {
        cy.get(inventarioElements.lbl_status).eq(0).contains('Em progresso');
    }

    validarTelaAnaliseConfronto() {
        cy.get(inventarioElements.btn_finalizar).should('be.visible');
        cy.get(inventarioElements.txt_segunda_contagem).should('be.visible');
    }

    validarTabelaInventario() {
        cy.get(inventarioElements.tab_inventario).should('be.visible');
        cy.get(inventarioElements.col_title).should('be.visible');
    }

    validarDownloadArquivoInventario() {
        const data = [
            "SKU",
            "EAN",
            "Titulo",
            "Seção",
            "Primeira contagem",
            "Segunda contagem",
        ];
        basePage.getFilenameAndValidateXlsx(data);
    }

    validarDownloadArquivoSecaoInventario() {
        const data = [
            "SKU",
            "EAN",
            "Titulo",
            "Quantidade",
        ];
        basePage.getFilenameAndValidateXlsx(data);
    }

    validarBtnUpload() {
        cy.get(inventarioElements.btn_upload).should('be.enabled');
    }

    validarCheckColunaDiferenca() {
        cy.get(inventarioElements.tr_inventario)
            .parent()
            .find(inventarioElements.icon_check).should('be.visible');
    }

    validarSwitch(valor) {
        cy.get(inventarioElements.icon_switch_sim_nao).should('be.visible').as('switch_sim_nao');
        switch (valor) {
            case 'Sim':
                cy.get('@switch_sim_nao')
                    .contains('Sim');
                break;
            case 'Não':
                cy.get('@switch_sim_nao')
                    .contains('Não');
                break;
            default:
                break;
        }
    }

    validarAbaItensNaoContabilizados(valor) {
        switch (valor) {
            case 'Sim':
                cy.get(inventarioElements.txt_segunda_contagem).should('be.visible');
                break;
            case 'Não':
                cy.get(inventarioElements.lbl_nao_existe_dados).contains('Não há dados');
                break;
            default:
                break;
        }
    }
}

export default new inventarioPage();