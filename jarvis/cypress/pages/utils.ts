export const utilsElm = {
  main: "#root",
  open_list_store: ".ant-select-selection-selected-value",
  input_store: 'input[class="ant-select-search__field"]',
  stores: 'ul[role="listbox"] > li[role="option"]',
};

export class Utils {
  generateDates(typeDate?: string) {
    if (typeDate === "now") {
      let datesNow = new Date();
      let dia = datesNow.getDate().toString().padStart(2, "0");
      let mes = (datesNow.getMonth() + 1).toString().padStart(2, "0");
      let ano = datesNow.getFullYear();
      let date = dia + "/" + mes + "/" + ano;

      return date;
    } else {
    }
  }

  selectStore(data?: any) {
    cy.get(utilsElm.open_list_store).click();
    if (typeof data == "boolean") {
      cy.get(utilsElm.stores)
        .its("length")
        .then((length) => {
          let storeRandom = Math.floor(Math.random() * length);
          cy.get(utilsElm.stores).eq(storeRandom).click();
        });
    } else {
      cy.get(utilsElm.input_store).type(data);
      cy.get(utilsElm.main).click({ force: true });
    }
  }
}

export default new Utils();
