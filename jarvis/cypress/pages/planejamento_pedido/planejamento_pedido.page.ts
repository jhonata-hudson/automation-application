/// <reference path="../../support/index.d.ts" />

import basePage from '../base/basePage';
import planejamentoPedidosElements from './planejamento_pedido.elements';

class planejamentoPedidoPage {
    //Eventos de Click()
    clicarMenuPlanejamentoPedido() {
        cy.get(planejamentoPedidosElements.nav_plan_ped)
            .parent()
            .contains('Planejamento de Pedido')
            .click();
    }

    clicarMenuCadastroPedidos() {
        cy.get(planejamentoPedidosElements.li_cadastro_plan_ped).click();
    }

    clicarMenuListaPedidos() {
        cy.get(planejamentoPedidosElements.li_listar_plan_ped).click();
    }

    clicarCriarGrupo() {
        cy.get(planejamentoPedidosElements.li_criar_grupo_plan_ped).click();
    }

    clicarAtualizarGrupo() {
        cy.get(planejamentoPedidosElements.li_atualizar_grupo_plan_ped).click();
    }

    clicarAtualizarCoberturaLojas() {
        cy.get(planejamentoPedidosElements.li_atualizar_cobertura_lojas_plan_ped).click();
    }

    clicarAtualizarSKUs() {
        cy.get(planejamentoPedidosElements.nav_skus)
            .parent()
            .contains('SKUs')
            .click();
        cy.get(planejamentoPedidosElements.li_atualizar_skus).click();
    }

    clicarLiberarPedidos() {
        cy.get(planejamentoPedidosElements.btn_pedidos).children().contains('Liberar Pedidos').click({ force: true });
        cy.get(planejamentoPedidosElements.modal_liberar_pedidos).click().contains('OK').click();
    }

    clicarEncerrarPedidos() {
        cy.get(planejamentoPedidosElements.btn_pedidos).contains('Encerrar Pedidos').click({ force: true });
    }

    clicarIconeDownload() {
        cy.get(planejamentoPedidosElements.tr_lista_pedidos).eq(0).get(planejamentoPedidosElements.icon_download_pedidos).eq(0)
            .should("be.visible")
            .click();
    }

    clicarBtnCriarGrupo() {
        cy.get(planejamentoPedidosElements.btn_grupo).click();
    }

    clicarModalConfimarCriarGrupo() {
        cy.get(planejamentoPedidosElements.modal_confirmar_grupo).contains('Tem certeza que deseja criar um novo grupo?').should('be.visible');
        cy.contains('Sim').click();
    }

    clicarModalConfimarAtualizarGrupo() {
        cy.get(planejamentoPedidosElements.modal_confirmar_grupo).contains('Tem certeza que deseja atualizar o grupo').should('be.visible');
        cy.contains('Sim').click();
    }

    clicarBtnEnviar() {
        cy.get(planejamentoPedidosElements.btn_enviar).click();
    }

    // Eventos de Seleção
    selecionarCriarTipoGrupo(tipo_grupo) {
        cy.get(planejamentoPedidosElements.cbo_criar_tipo_grupo).contains('Tipo de grupo').should('be.visible').as('combo_tipo_grupo');
        switch (tipo_grupo) {
            case 'Prateleiras':
                cy.get('@combo_tipo_grupo')
                    .click()
                    .type('{downarrow}{enter}');
                break;
            case 'Pedido de Compra':
                cy.get('@combo_tipo_grupo')
                    .click()
                    .type('{downarrow}{downarrow}{enter}')
                break;
            default:
                break;
        }
    }

    selecionarAtualizarTipoGrupo(tipo_grupo) {
        cy.get(planejamentoPedidosElements.cbo_atualizar_tipo_grupo).contains('Tipo de grupo').should('be.visible').as('combo_tipo_grupo');
        switch (tipo_grupo) {
            case 'Prateleiras':
                cy.get('@combo_tipo_grupo')
                    .click()
                    .type('{downarrow}{enter}');
                break;
            case 'Pedido de Compra':
                cy.get('@combo_tipo_grupo')
                    .click()
                    .type('{downarrow}{downarrow}{enter}')
                break;
            default:
                break;
        }
    }

    selecionarArquivos(arquivo) {
        cy.get(planejamentoPedidosElements.btn_upload).attachFile(arquivo, { allowEmpty: true });
    }

    // Eventos de Preenchimento de Infos
    preencherNomeGrupo(nome) {
        cy.get(planejamentoPedidosElements.txt_nome_grupo).type(nome);
    }

    preencherListaLojas(loja) {
        cy.get(planejamentoPedidosElements.txt_lista_lojas).type(loja);
    }

    preencherListaSKU(sku) {
        cy.get(planejamentoPedidosElements.txt_lista_sku).type(sku);
    }

    // Eventos de Busca
    buscarNomeGrupo(nome) {
        cy.get(planejamentoPedidosElements.txt_nome_grupo).type(nome + '{enter}');
    }

    // Eventos de Validação
    validarTabelaListaPedidos() {
        cy.get(planejamentoPedidosElements.tab_lista_pedidos).should('be.visible');
        cy.get(planejamentoPedidosElements.col_title_lista_pedidos).should('be.visible');
    }

    validarSucessoPedidoLiberado() {
        cy.get(planejamentoPedidosElements.btn_pedidos, { timeout: 20000 }).should('have.text', 'Encerrar Pedidos');
    }

    validarSucessoPedidoEncerrado() {
        cy.get(planejamentoPedidosElements.check_circle).should('be.visible');
        cy.get(planejamentoPedidosElements.lbl_pedidos_encerrados).contains('Pedidos encerrados');
    }

    validarDownloadArquivoListaPedidos() {
        const data = [
            "Produto",
            "SKU",
            "Preço Custo",
            "Preço Venda",
            "Qtde. Limite máximo",
            "Qtde. Pedido",
            "Qtde. Faturado",
            "Qtde. Recebido"
        ];
        basePage.getFilenameAndValidateXlsx(data);
    }
}

export default new planejamentoPedidoPage();