export const planejamentoPedidoElements = {
    // Menu Planejamento de Pedidos
    nav_plan_ped: 'i[class$="fund"]',
    li_listar_plan_ped: 'a[href="/restock-planning/list"]',
    li_cadastro_plan_ped: 'a[href="/restock-planning/register"]',
    li_criar_grupo_plan_ped: 'a[href="/restock-planning/create-group"]',
    li_atualizar_grupo_plan_ped: 'a[href="/restock-planning/update-group"]',
    li_atualizar_cobertura_lojas_plan_ped: 'a[href="/restock-planning/update-distributor-coverage"]',
    nav_skus: 'i[class$="skin"]',
    li_atualizar_skus: 'a[href="/skus/update-by/upload-xlsx"]',

    // Tela Cadastro de Pedidos
    btn_pedidos: 'button[class$="ant-btn-primary"]',
    check_circle: 'i[class*="check-circle"]',
    lbl_pedidos_encerrados: 'h3[class="ant-typography"]',
    modal_liberar_pedidos: 'div[class$="ant-modal-content"]',

    // Tela Lista de Pedidos
    tab_lista_pedidos: 'div[class="ant-table-body"]',
    col_title_lista_pedidos: 'span[class="ant-table-column-title"]',
    tr_lista_pedidos: 'tr[class$="ant-table-row-level-0"]',
    icon_download_pedidos: 'button[class$="ant-btn-link"]',

    // Tela Criar Grupo // Tela Atualizar Grupo
    cbo_criar_tipo_grupo: 'div[role="combobox"]',
    cbo_atualizar_tipo_grupo: 'div.ant-col.ant-col-6 > div',
    txt_nome_grupo: 'input[placeholder*="do grupo"]',
    txt_lista_lojas: 'textarea[name="distributors"]',
    txt_lista_sku: 'textarea[name="skus"]',
    btn_grupo: 'button[type="submit"]',
    modal_confirmar_grupo: 'div[class="ant-modal-body"]',

    // Tela Atualizar Cobertura / SKUs
    btn_upload: 'input[name="arqExcel"]',
    btn_enviar: 'button[class*="ant-btn-primary"]',
}

export default planejamentoPedidoElements;