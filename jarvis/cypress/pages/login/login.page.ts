/// <reference path="../../support/index.d.ts" />

import baseElements from '../base/baseElements';
import loginElements from './login.elements';

class loginPage {
    acessarLogin() {
        cy.visit('/login');
    }

    //Eventos de Click()
    clicarBtnEntrar() {
        cy.get(loginElements.btn_entrar).click();
    }

    clicarEsqueciSenha() {
        cy.get(loginElements.btn_esqueci_senha).click();
    }

    // Eventos de Preenchimento de Infos
    preencherUsuario(usuario) {
        cy.get(loginElements.txt_usuario).type(usuario);
    }

    preencherSenha(senha) {
        cy.get(loginElements.txt_senha).type(senha, { log: false });
    }

    // Eventos de Validação
    validarLogin() {
        return cy.get(baseElements.cbo_lojas);
    }
}

export default new loginPage();