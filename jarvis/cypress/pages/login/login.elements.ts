export const loginElements = {
    // Tela Login
    txt_usuario: 'input[type=email]',
    txt_senha: 'input[type=password]',
    btn_entrar: 'button[type=submit]',
    btn_esqueci_senha: 'a[style$="right;"]'
}

export default loginElements;