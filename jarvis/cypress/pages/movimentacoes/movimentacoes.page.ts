/// <reference path="../../support/index.d.ts" />

import { movElements } from "./movimentacoes.elements";
import utils from "./../utils";

export const months = {
  jan: "janeiro",
  fev: "fevereiro",
  mar: "março",
  abr: "abril",
  mai: "maio",
  jun: "jun",
  jul: "julho",
  ago: "agosto",
  set: "setembro",
  out: "outubro",
  nov: "novembro",
  dez: "dezembro",
};

export const dropdownObj = {
  typeInvoice: 0,
  typeRequests: 2,
};

export const filterObj = {
  typeInvoice: 0,
  typeRequests: 1,
};

export function propPrefix(str) {
  return str;
}

export class Movimentacoes {
  inputDocumentKey(key: string) {
    cy.get(movElements.div_main_input_invoice)
      .last()
      .find(movElements.input_document_key)
      .type(key);
  }

  calendar(date: string) {
    cy.get(movElements.icon_calendar).trigger("mouseover", {
      force: true,
    });
    cy.get(movElements.clear_dates, { timeout: 3000 }).click({
      force: true,
    });
    cy.get(movElements.click_input_dates).first().click({ force: true });
    cy.get(movElements.panel_calendar).then(() => {
      let dateSplit = date.split("-");
      let dateFrag = [
        [dateSplit[0], "right"],
        [dateSplit[1], "left"],
      ];
      let cont = 0;

      dateFrag.forEach((opt) => {
        console.log("opt", opt);
        this.calendarInteract(opt, cont);
        cont++ + 1;
      });
    });
  }

  calendarHeader(date: string, index: number) {
    let year = date.split(" ")[0];
    let month = date.split(" ")[1];
    let datesData = [year, month];

    cy.get(movElements.header_calendar, { timeout: 3000 })
      .eq(index)
      .then((getIn) => {
        cy.get(movElements.sub_tag_header_calendar).then((res) => {
          let elements = [movElements.select_year, movElements.select_month];

          let cont = 0;
          elements.forEach((el) => {
            cy.get(el).eq(index).click({ force: true });

            let type = el.split("-")[2];

            cy.get(`div[class="ant-calendar-${type}-panel"]`)
              .find(`.ant-calendar-${type}-panel-${type}`)
              .contains(new RegExp("^" + datesData[cont] + "$", "g"))
              .click({ force: true });

            cont++ + 1;
          });
        });
      });
  }

  calendarBody(title: string, index: number) {
    cy.get(movElements.body_days_calendar, { timeout: 3000 })
      .eq(index)
      .get(movElements.sub_body_calendar)
      .eq(index)
      .find(`td[title="${title}"]`)
      .click({ force: true });
  }

  calendarInteract(dates?: any, index?: number) {
    let day = dates[0].split(" ")[2];
    let year = dates[0].split(" ")[0];
    let month = dates[0].split(" ")[1];

    cy.get(`div[class$="ant-calendar-range-${dates[1]}"]`).then(() => {
      this.calendarHeader(dates[0], index);
      let prop = propPrefix(month);
      let title = `${day} de ${months[prop]} de ${year}`;
      this.calendarBody(title, index);
    });
  }

  visitMov(endPoint?: string) {
    cy.visit(`/invoices/history/${endPoint}`);
  }

  useCalendarOrDocumentKey(typeSearch?: any, data?: any) {
    
    typeof typeSearch === "object"
      ? this.inputDocumentKey(typeSearch[data])
      : this.calendar(typeSearch);

    cy.get(movElements.button_search_invoice).first().click({ force: true });
  }

  filterInvoices(filter?: string, optFilter?: string) {
    let prop = propPrefix(optFilter);
    let propdropdownObj = propPrefix(optFilter);
    let index = filterObj[prop];
    let indexdropdown = dropdownObj[propdropdownObj];

    cy.get(movElements.button_filter).eq(indexdropdown).click();

    if (filter === "random") {
      cy.get(movElements.drop_list)
        .eq(index)
        .find("li")
        .its("length")
        .then((res) => {
          let eqRandom = Math.floor(Math.random() * res);
          cy.get(movElements.drop_list)
            .eq(index)
            .find("li")
            .eq(eqRandom)
            .trigger("click");
        })
        .invoke("text")
        .as("text");
      cy.get(movElements.confirm).eq(index).click();

      cy.get("@text").then((results) => {
        this.validFilterResults(results, indexdropdown);
      });
    }
  }

  validFilterResults(result?: any, optFilter?: number) {
    console.log("result", result, optFilter);
    cy.get(movElements.list_invoices)
      .its("length")
      .then((amountNF) => {
        if (amountNF != 0) {
          for (let index = 0; index < amountNF; index++) {
            cy.get(movElements.list_invoices)
              .eq(index)
              .then((within) => {
                for (let index = 0; index < amountNF; index++) {
                  let text = within
                    .find(movElements.filter_colum)
                    .eq(optFilter)
                    .text();
                    console.log("text", text);

                  if (text != result) {
                    throw new Error("Filtro com resultados diferentes");
                  }
                }
              });
          }
        } else {
          cy.log(String("Nenhuma nota foi encontrada !!!"));
        }
      });
  }

  validNF() {
    //So deve retornar erro em consultas de chave de acesso validas
    cy.get(movElements.list_invoices).should("be.visible");
  }
}

export default new Movimentacoes();
