export const movElements = {
  // calendario

  endDate: "ant-row ant-form-item item",
  button_search_invoice:
    'button[type="button"][class="ant-btn ant-btn-primary"]',
  div_main_input_invoice: ".ant-row.ant-form-item.item",
  input_document_key: ".ant-col.ant-form-item-control-wrapper>div>span>input",
  get_in: ".ant-row.ant-form-item.item",
  div_item: ".ant-row.ant-form-item.item",
  div_calendar: ".ant-col.ant-form-item-control-wrapper",
  sub_div_calendar: ".ant-form-item-control",
  icon_calendar: ".ant-calendar-picker-icon",
  clear_dates: ".ant-calendar-picker-clear",
  click_input_dates: 'input[class="ant-calendar-range-picker-input"]',
  panel_calendar: 'div[class="ant-calendar-date-panel"]',
  header_calendar: 'div[class="ant-calendar-header"]',
  sub_tag_header_calendar: ".ant-calendar-ym-select",
  select_year: 'a[class="ant-calendar-year-select"]',
  select_month: 'a[class="ant-calendar-month-select"]',
  body_days_calendar: 'div[class="ant-calendar-body"]',
  sub_body_calendar: ".ant-calendar-tbody",
  results_invoices: ".ant-table-content",
  /// FILTROS
  button_filter: ".ant-dropdown-trigger",
  drop_list: ".ant-dropdown-menu-vertical",
  body_table: ".ant-table-tbody",
  list_invoices: ".ant-table-row.ant-table-row-level-0",
  filter_colum: ".ant-table-column-has-filters",
  confirm: ".confirm"

};

export default movElements;
