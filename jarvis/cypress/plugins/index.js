/// <reference types="cypress" />
/// <reference types="@shelex/cypress-allure-plugin" />
// ***********************************************************
/**
 * @type {Cypress.PluginConfig}
 */
// ***********************************************************
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const xlsx = require('node-xlsx').default;
const fs = require('fs');

module.exports = (config) => {
  require('cypress-grep/src/plugin')(config)
}

module.exports = (on, config) => {
  on('task', {
    parseXlsx({ filePath }) {
      return new Promise((resolve, reject) => {
        try {
          const jsonData = xlsx.parse(fs.readFileSync(filePath));
          resolve(jsonData);
        } catch (e) {
          reject(e);
        }
      });
    },
    readFiles({ path }) {
      return new Promise((res, reject) => {
        fs.readdir(path, (err, filenames) => {
          if (err) {
            reject(err);
          }
          res(filenames);
        });
      })
    }
  }),
    allureWriter(on, config)
  return config;
}