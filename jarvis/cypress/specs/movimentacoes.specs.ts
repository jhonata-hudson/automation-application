import utils from "./../pages/utils";
import basePage from "../pages/base/basePage";
import movPage from "../pages/movimentacoes/movimentacoes.page";

beforeEach(() => {
  basePage.login();
});

describe(
  "Movimentacoes",
  { tags: ["regression", "dev", "movimentacoes"] },
  () => {
    // Consulta de notas de Entrada

    it("Consulta de notas de entrada por data inicio fim", () => {
      movPage.visitMov("in");
      utils.selectStore("1024"); // trocar para true para selecao randomica
      movPage.useCalendarOrDocumentKey("2021 fev 1-2022 fev 18");
      movPage.validNF();
      //Exemplo de como inserir data no parametro (data inicio >> ano mes dia-ano mes dia)
    });

    it("Consulta de notas de entrada por chave de acesso Fiscals", () => {
      cy.fixture("movimentacoes/documentKey").then((res) => {
        movPage.visitMov("in");
        utils.selectStore("1024"); // trocar para true para selecao randomica
        movPage.useCalendarOrDocumentKey(res, "invoiceFiscals");
        movPage.validNF();
      });
    });

    //Consulta de notas de saida

    it("Consulta de notas de saida por data inicio fim", () => {
      movPage.visitMov("out"); //Exemplo de como inserir data no parametro (data inicio >> ano mes dia-ano mes dia)
      utils.selectStore("1024"); // trocar para true para selecao randomica
      movPage.useCalendarOrDocumentKey("2021 fev 1-2022 fev 18");
      movPage.validNF();
    });

    it("Consultar notas de saida e filtrar por tipos de nota", () => {
      movPage.visitMov("out");
      utils.selectStore("1024");
      movPage.useCalendarOrDocumentKey("2021 fev 1-2022 fev 18");
       // trocar para true para selecao randomica
      movPage.filterInvoices("random", "typeInvoice");
    });

    it("Consultar notas de saida e filtrar por tipos de nota", () => {
      movPage.visitMov("out");
      utils.selectStore("1024");
      movPage.useCalendarOrDocumentKey("2021 fev 1-2022 fev 18");
      movPage.filterInvoices("random", "typeRequests");
    });
  }
);
