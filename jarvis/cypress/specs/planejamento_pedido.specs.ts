/// <reference path="../support/commands.ts" />

import basePage from '../pages/base/basePage';
import planejamento_pedidoPage from '../pages/planejamento_pedido/planejamento_pedido.page';

beforeEach(() => {
    basePage.login();
})

describe('Planejamento de Pedido', { tags: ['regression', 'dev', 'planejamento_pedido'] }, () => {
    it('Encerrar pedidos', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarMenuCadastroPedidos();
        planejamento_pedidoPage.clicarEncerrarPedidos();
        planejamento_pedidoPage.validarSucessoPedidoEncerrado();
    });

    it('Liberar pedidos', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarMenuCadastroPedidos();
        planejamento_pedidoPage.clicarLiberarPedidos();
        planejamento_pedidoPage.validarSucessoPedidoLiberado();
    });

    it('Visualizar lista de pedidos', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarMenuListaPedidos();
        planejamento_pedidoPage.validarTabelaListaPedidos();
    });

    it('Realizar download da planilha de um pedido realizado', () => {
        basePage.deletarArquivosDownloads();
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarMenuListaPedidos();
        planejamento_pedidoPage.clicarIconeDownload();
        planejamento_pedidoPage.validarDownloadArquivoListaPedidos();
    });

    it('Criar um grupo do tipo Prateleiras', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('4001');
        planejamento_pedidoPage.preencherListaSKU('COLOBTBAZCL3045');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Grupo grupo_teste_prateleiras criado com sucesso!');
    });

    it('Não deve criar um grupo do tipo Prateleiras sem informar um nome do grupo', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O campo "nome" é obrigatório');
    });

    it('Não deve criar um grupo do tipo Prateleiras ao informar uma Lista de Lojas inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('XXXX');
        planejamento_pedidoPage.preencherListaSKU('COLOBTBAZCL3045');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Não deve criar um grupo do tipo Prateleiras ao informar uma Lista de SKU inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('4001');
        planejamento_pedidoPage.preencherListaSKU('XXXX');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Criar um grupo do tipo Pedido de Compra', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('4001');
        planejamento_pedidoPage.preencherListaSKU('COLOBTBAZCL3045');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Grupo grupo_teste_ped_com criado com sucesso!');
    });

    it('Não deve criar um grupo do tipo Pedido de Compra sem informar um nome do grupo', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O campo "nome" é obrigatório');
    });

    it('Não deve criar um grupo do tipo Pedido de Compra ao informar uma Lista de Lojas inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('XXXX');
        planejamento_pedidoPage.preencherListaSKU('COLOBTBAZCL3045');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Não deve criar um grupo do tipo Pedido de Compra ao informar uma Lista de SKU inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarCriarGrupo();
        planejamento_pedidoPage.selecionarCriarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.preencherNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('4001');
        planejamento_pedidoPage.preencherListaSKU('XXXX');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarCriarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Atualizar um grupo do tipo Prateleiras', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('\n2556');
        planejamento_pedidoPage.preencherListaSKU('\nCOLOBTBAZCL1035');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Grupo grupo_teste_prateleiras atualizado com sucesso!');
    });

    it('Não deve atualizar um grupo do tipo Prateleiras ao informar uma Lista de Lojas inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('\nXXXX');
        planejamento_pedidoPage.preencherListaSKU('\nCOLOBTBAZCL1035');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Não deve atualizar um grupo do tipo Prateleiras ao informar uma Lista de SKU inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Prateleiras');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_prateleiras');
        planejamento_pedidoPage.preencherListaLojas('\n2556');
        planejamento_pedidoPage.preencherListaSKU('\nXXXX');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Atualizar um grupo do tipo Pedido de Compra', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('\n2556');
        planejamento_pedidoPage.preencherListaSKU('\nCOLOBTBAZCL1035');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Grupo grupo_teste_ped_com atualizado com sucesso!');
    });

    it('Não deve atualizar um grupo do tipo Pedido de Compra ao informar uma Lista de Lojas inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('\nXXXX');
        planejamento_pedidoPage.preencherListaSKU('\nCOLOBTBAZCL1035');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Não deve atualizar um grupo do tipo Pedido de Compra ao informar uma Lista de SKU inválida', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarGrupo();
        planejamento_pedidoPage.selecionarAtualizarTipoGrupo('Pedido de Compra');
        planejamento_pedidoPage.buscarNomeGrupo('grupo_teste_ped_com');
        planejamento_pedidoPage.preencherListaLojas('\n2556');
        planejamento_pedidoPage.preencherListaSKU('\nXXXX');
        planejamento_pedidoPage.clicarBtnCriarGrupo();
        planejamento_pedidoPage.clicarModalConfimarAtualizarGrupo();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!SKU ou Loja inválida.');
    });

    it('Atualizar a cobertura das lojas ao importar um arquivo excel', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarCoberturaLojas();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_cobertura_lojas.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Dados Atualizados com sucesso.');
    });

    it('Não deve atualizar a cobertura das lojas ao importar um arquivo diferente de excel', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarCoberturaLojas();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_cobertura_lojas.pdf');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Há coluna(s) id da loja vazia(s)');
    });

    it('Não deve atualizar a cobertura das lojas ao importar um arquivo excel sem uma coluna obrigatória', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarCoberturaLojas();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_cobertura_lojas_sem_col_obrigatoria.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Há coluna(s) id da loja vazia(s)');
    });

    it('Não deve atualizar a cobertura das lojas ao importar um arquivo excel vazio', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarCoberturaLojas();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_cobertura_lojas_vazio.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!A planilha está vazia');
    });

    it('Não deve atualizar a cobertura das lojas ao importar um arquivo excel com valores inválidos', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarCoberturaLojas();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_cobertura_lojas_dados_invalidos.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Algo deu errado. Estamos trabalhando para deixar tudo funcionando novamente.');
    });

    it('Atualizar os skus ao importar um arquivo excel', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarSKUs();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_skus.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!2 SKUs foram atualizados com sucesso.');
    });

    it('Não deve atualizar skus ao importar um arquivo diferente de excel', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarSKUs();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_skus.pdf');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!As seguintes colunas não são aceitas: %PDF-1.4');
    });

    it('Não deve atualizar skus ao importar um arquivo excel sem uma coluna obrigatória', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarSKUs();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_skus_sem_col_obrigatoria.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Algo deu errado. Estamos trabalhando para deixar tudo funcionando novamente.');
    });
    it('Não deve atualizar skus ao importar um arquivo excel vazio', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarSKUs();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_skus_lojas_vazio.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O XLSX está vazio');
    });

    it('Não deve atualizar skus ao importar um arquivo excel com valores inválidos', () => {
        planejamento_pedidoPage.clicarMenuPlanejamentoPedido();
        planejamento_pedidoPage.clicarAtualizarSKUs();
        planejamento_pedidoPage.selecionarArquivos('planejamento_pedido/atualizar_skus_dados_invalidos.xlsx');
        planejamento_pedidoPage.clicarBtnEnviar();
        basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!As seguintes colunas não são aceitas: mirroredSku para skus com origem no Protheus');
    });
})