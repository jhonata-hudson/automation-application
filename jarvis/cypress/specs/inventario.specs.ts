/// <reference path="../support/commands.ts" />

import basePage from '../pages/base/basePage';
import inventarioPage from '../pages/inventario/inventario.page';

beforeEach(() => {
  basePage.login();
})

describe('Inventário', { tags: ['regression', 'dev', 'inventario'] }, () => {
  it('Realizar um inventário', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.preencherCamposSegundaContagem();
    inventarioPage.clicarBtnFinalizar();
    inventarioPage.clicarFinalizarInventario();
    inventarioPage.validarSucessoInvetario();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Inventário finalizado com sucesso');
  })

  it('Visualizar lista de inventários', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.validarTabelaInventario();
  })

  it('Realizar download da planilha de um inventário realizado', () => {
    basePage.deletarArquivosDownloads();
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarIconeDownload();
    inventarioPage.validarDownloadArquivoInventario();
  })

  it('Não deve permitir que usuário continue o inventário sem selecionar uma loja', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'AtençãoUma loja deve ser selecionada');
  })

  it('Não deve permitir que usuário continue o inventário sem selecionar um arquivo txt', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Cuidado!É necessário incluir um arquivo para continuar');
  })

  it('Não deve aceitar importar um arquivo diferente de txt/xlsx/xls', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.pdf');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O tipo de arquivo não é permitido');
  })

  it('Não deve permitir que usuário continue o inventário ao anexar um arquivo vazio', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos_vazio.txt');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O arquivo de inventário está vazio');
  });

  it('Validar que ao informar valores inválidos no arquivo exibe mensagem de erro', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos_dados_invalidos.txt');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O valor do campo "Seção" na linha 3 está inválido');
  });

  it('Não deve permitir que usuário continue o inventário informando uma loja diferente do arquivo anexado', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1003');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!A loja encontrada no arquivo "1081" é diferente da loja selecionada "1003".');
  });

  it('Validar que o usuário após importar um arquivo consegue removê-lo', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarRemoverArquivo();
    inventarioPage.validarBtnUpload();
  })

  it('Validar que o default é sim para o switch "Carregar produtos com saldo não contabilizado pelo coletor?"', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.validarSwitch('Sim');
  })

  it('Validar que o usuário consegue retomar um inventário que já está em progresso', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarIconeVoltar();
    inventarioPage.clicarBtnEditar();
    inventarioPage.validarTelaAnaliseConfronto();
  })

  it('Não deve criar um novo inventário quando já existir um em progresso', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Já existe um inventário em progresso para a loja 1081');
  })

  it('Realizar download da planilha de resultados de um inventário', () => {
    basePage.deletarArquivosDownloads();
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarIconeImprimirResultados();
    inventarioPage.validarDownloadArquivoInventario();
  })

  it('Realizar download da planilha de uma seção', () => {
    basePage.deletarArquivosDownloads();
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarImprimirResultadosSecao();
    inventarioPage.validarDownloadArquivoSecaoInventario();
  })

  it('Exibir um check na coluna diferença na aba Combinados quando não houver divergência no saldo do SKU', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarAbaCombinados();
    inventarioPage.validarCheckColunaDiferenca();
  })

  it('Validar que switch "Sim" mostra os produtos com saldo no sistema na aba "Itens não contablizados"', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.validarSwitch('Sim');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarAbaItensNaoContabilizados();
    inventarioPage.validarAbaItensNaoContabilizados('Sim')
    inventarioPage.clicarBtnFinalizar();
    inventarioPage.clicarFinalizarInventario();
    inventarioPage.validarSucessoInvetario();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Inventário finalizado com sucesso');
  });

  it('Validar que switch "Não" não mostra nenhum produto a mais a não ser o do arquivo importado', () => {
    inventarioPage.clicarMenuInventario();
    inventarioPage.clicarBtnNovoInvetario();
    inventarioPage.selecionarLoja('1081');
    inventarioPage.selecionarArquivo('inventario/mmartan_shopping_villa_lobos.txt');
    inventarioPage.clicarSwitchNao();
    inventarioPage.validarSwitch('Não');
    inventarioPage.clicarBtnContinuar();
    inventarioPage.clicarAbaItensNaoContabilizados();
    inventarioPage.validarAbaItensNaoContabilizados('Não');
    inventarioPage.clicarBtnFinalizar();
    inventarioPage.clicarFinalizarInventario();
    inventarioPage.validarSucessoInvetario();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!Inventário finalizado com sucesso');
  });
})