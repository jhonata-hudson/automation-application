import basePage from '../pages/base/basePage';
import loginPage from '../pages/login/login.page';

describe('Login', { tags: ['regression', 'dev', 'login'] }, () => {
  const email = Cypress.env('email')
  const senha = Cypress.env('password')

  it('Login com sucesso', () => {
    loginPage.acessarLogin();
    loginPage.preencherUsuario(email);
    loginPage.preencherSenha(senha);
    loginPage.clicarBtnEntrar();
    loginPage.validarLogin().should('be.visible');
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Olá, Raquel LimaBem-vindo ao Painel Omni!');
  })

  it('Recuperação de Senha', () => {
    loginPage.acessarLogin();
    loginPage.preencherUsuario(email);
    loginPage.clicarEsqueciSenha();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Sucesso!E-mail enviado');
  })

  it('Login com email incorreto', () => {
    loginPage.acessarLogin();
    loginPage.preencherUsuario('teste@gmail.com');
    loginPage.preencherSenha(senha);
    loginPage.clicarBtnEntrar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Usuário não encontrado');
  })

  it('Login com senha incorreta', () => {
    loginPage.acessarLogin();
    loginPage.preencherUsuario(email);
    loginPage.preencherSenha('teste123');
    loginPage.clicarBtnEntrar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!Ops! Senha incorreta');
  })

  it('Campo e-mail não informado', () => {
    loginPage.acessarLogin();
    loginPage.clicarBtnEntrar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O campo "e-mail" é obrigatório');
  })

  it('Campo senha não informado', () => {
    loginPage.acessarLogin();
    loginPage.preencherUsuario(email);
    loginPage.clicarBtnEntrar();
    basePage.validarAlertaMensagem().should('be.visible').and('text', 'Erro!O campo "senha" é obrigatório');
  })
})