import '@testing-library/cypress/add-commands';
import 'cypress-file-upload';
// ***********************************************************
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// ***********************************************************
Cypress.Commands.add('logindevbackoffice', (
    email = Cypress.env('email'),
    senha = Cypress.env('password')
) => {
    cy.session('login', () => {
        cy.visit('/login');
        cy.get('input[type=email]').type('jhonata.oliveira@coteminas.com.br');
        cy.get('input[type=password]').type('mudar123', { log: false });
        cy.get('button[type=submit]').click();
        cy.get('div[role="combobox"]').should('be.visible');
    });
})

Cypress.Commands.add("parseXlsx", (inputFile) => {
    return cy.task('parseXlsx', { filePath: inputFile })
});

Cypress.Commands.add("readFiles", (path) => {
    return cy.task('readFiles', { path })
});