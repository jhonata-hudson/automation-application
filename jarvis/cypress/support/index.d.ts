// load type definitions from Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
        /**
         * Custom command to visit BackOffice AMMO Varejo - Desenvolvimento
         * @example cy.logindevbackoffice()
         */
        logindevbackoffice(): Chainable<Window>
    }
}

declare namespace Cypress {
    interface Chainable {
        /**
         * Custom command to visit BackOffice AMMO Varejo - Desenvolvimento
         * @example cy.logindevbackoffice()
         */
        requestlogin(): Chainable<Window>
    }
}

declare namespace Cypress {
    // specify additional properties in the TestConfig object
    // in our case we will add "tags" property
    interface TestConfigOverrides {
        /**
         * List of tags for this test
         * @example a single tag
         *  it('logs in', { tags: '@smoke' }, () => { ... })
         * @example multiple tags
         *  it('works', { tags: ['@smoke', '@slow'] }, () => { ... })
         */
        tags?: string | string[]
    }
}

declare namespace Cypress {
    interface Chainable {
        /**
         * Custom command to get filename
         * @example cy.readFiles()
         */
        readFiles(path): Chainable<any>
    }
}

declare namespace Cypress {
    interface Chainable {
        /**
         * Custom command to parse our excel file and convert it to json
         * @example cy.parseXlsx()
         */
        parseXlsx(inputFile): Chainable<File>
    }
}