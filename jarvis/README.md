# Jarvis

![jarvis](https://qph.fs.quoracdn.net/main-qimg-60cc4742c2efddbe6b5c45347bd1c905)

> Jarvis é um projeto baseado em [Cypress](https://www.cypress.io/) para testes end-to-end do [Backoffice](https://bitbucket.org/projeto-omni/jarvis/src/master/)

## Índice

- Instalação
- Pré-Requisitos
- Execução dos Testes
- Guia de Contribuição

## Instalação

```shell
git clone https://bitbucket.org/projeto-omni/jarvis/src/master/
cd jarvis
npm install
```

## Pré-Requisitos
- [node js](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/docs/install)
- [cypress](https://www.cypress.io/)
- [allure](https://docs.qameta.io/allure/)

## Execução dos Testes
- open browser: `npm run cy:open`
- headless: `npm run test`
- allure:clear: `npm run allure:clear`
- allure:generate: `npm run allure:generate`
- allure:open: `npm run allure:open`
- tags: `yarn cypress run --env grepTags=nome_tags`

> Obs.: É necessário criar um arquivo localmente na raiz do projeto com suas credenciais de acesso do `backoffice` para que os testes sejam executados com sucesso, pois esse arquivo `cypress.env.json` não está sendo versionado no git.

Exemplo: 

```
{
    "email": "usuario@coteminas.com.br",
    "password": "teste123"
}
```

## Guia de Contribuição

> O projeto segue boas práticas e para contribuições serem aceitas e necessário seguí-las conforme [documentação](https://wiki.projetoomni.com/pt-br/pix/backoffice/qualidade/jarvis) no `backoffice`.